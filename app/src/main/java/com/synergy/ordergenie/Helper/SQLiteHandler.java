package com.synergy.ordergenie.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by 1132 on 27-09-2016.
 */

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "ordergenie";


    //ordergenie STOCKIST WISE producttable

    private static final String TABLE_STOCKIST_WISE_PRODUCTS = "stockist_wise_products";
    // STOCKIST WISE producttable Columns names
    private static final String F_KEY_SP_ID = "serial_no";
    private static final String F_KEY_SP_ITEMCODE = "Itemcode";
    private static final String F_KEY_SP_STOCKIST_ID = "Stockist_id";
    private static final String F_KEY_SP_PRODUCT_ID = "Product_ID";
    private static final String F_KEY_SP_ITEMNAME = "Itemname";
    private static final String F_KEY_SP_PACKSIZE = "Packsize";
    private static final String F_KEY_SP_MRP = "MRP";
    private static final String F_KEY_SP_RATE = "Rate";
    private static final String F_KEY_SP_STOCK = "Stock";
    private static final String F_KEY_SP_TYPE = "type";
    private static final String F_KEY_SP_MFGCODE = "MfgCode";
    private static final String F_KEY_SP_MFGNAME = "MfgName";
    private static final String F_KEY_SP_DOSEFORM = "DoseForm";
    private static final String F_KEY_SP_SCHEME = "Scheme";


    //ordergenie Salesman products producttable

    private static final String TABLE_SALESMAN_PRODUCTS = "salesman_products";
    // STOCKIST WISE producttable Columns names
    private static final String F_KEY_SS_ID = "serial_no";
    private static final String F_KEY_SS_ITEMCODE = "Itemcode";
    private static final String F_KEY_SS_STOCKIST_ID = "Stockist_id";
    private static final String F_KEY_SS_PRODUCT_ID = "Product_ID";
    private static final String F_KEY_SS_ITEMNAME = "Itemname";
    private static final String F_KEY_SS_PACKSIZE = "Packsize";
    private static final String F_KEY_SS_MRP = "MRP";
    private static final String F_KEY_SS_RATE = "Rate";
    private static final String F_KEY_SS_STOCK = "Stock";
    private static final String F_KEY_SS_TYPE = "type";
    private static final String F_KEY_SS_MFGCODE = "MfgCode";
    private static final String F_KEY_SS_MFGNAME = "MfgName";
    private static final String F_KEY_SS_DOSEFORM = "DoseForm";
    private static final String F_KEY_SS_SCHEME = "Scheme";


    //chemist order table master
    private static final String TABLE_LEGEND_MASTER = "LEGEND_MASTER";
    // chemist order table Columns names
    private static final String F_KEY_LM_ID = "id";
    private static final String F_KEY_LM_STOCKIST_ID = "stockistId";
    private static final String F_KEY_LM_LEGENDNAME = "LegendName";
    private static final String F_KEY_LM_COLOR_CODE = "ColorCode";
    private static final String F_KEY_LM_START_RANGE = "StartRange";
    private static final String F_KEY_LM_END_RANGE = "EndRange";
    private static final String F_KEY_LM_STOCKLEGENDID = "StockLegendID";
    private static final String F_KEY_LM_LEGENDMODE = "LegendMode";


    //chemist order table master
    private static final String TABLE_CHEMIST_ORDER = "chemist_order";
    // chemist order table Columns names
    private static final String F_KEY_COR_ID = "serial_no";
    private static final String F_KEY_COR_DOC_NO = "DOC_NO";
    private static final String F_KEY_COR_DOC_DATE = "Doc_Date";
    private static final String F_KEY_COR_STOCKIST_ID = "Stockist_Client_id";
    private static final String F_KEY_COR_ITEMS = "Items";
    private static final String F_KEY_COR_REMARK = "Remark";
    private static final String F_KEY_COR_ORDER_AMOUNT = "Amount";
    private static final String F_KEY_COR_ORDER_STATUS = "Status";
    private static final String F_KEY_COR_CREATEDON = "Createdon";
    private static final String F_KEY_COR_CREATEBY = "CreatedBy";


    //chemist cart table
    private static final String TABLE_CHEMIST_CART = "chemist_cart";
    // chemist cart table Columns names
    private static final String F_KEY_CC_ID = "serial_no";
    private static final String F_KEY_CC_DOC_NO = "DOC_NO";
    private static final String F_KEY_CC_STOCKISTID = "Stockist_Client_id";
    private static final String F_KEY_CC_ITEM_COUNT = "Items";
    private static final String F_KEY_CC_ORDER_AMOUNT = "Amount";
    private static final String F_KEY_CC_REMARK = "Remarks";
    private static final String F_KEY_CC_ORDER_PLACED = "order_placed";
    private static final String F_KEY_CC_ORDER_SYNC = "order_sync";
    private static final String F_KEY_CC_DOC_DATE = "Doc_Date";
    private static final String F_KEY_CC_STATUS = "status";
    private static final String F_KEY_CC_CREATED_ON = "Createdon";


    //chemist cart DETAIL table master
    private static final String TABLE_CHEMIST_CART_DETAIL = "chemist_cart_detail";
    // chemist order DETAIL table Columns names
    private static final String F_KEY_CORD_ID = "serial_no";
    private static final String F_KEY_CORD_DOC_NO = "DOC_NO";
    private static final String F_KEY_CORD_DOC_ITEM_NO = "Doc_item_No";
    private static final String F_KEY_CORD_PRODUCT_ID = "Product_ID";
    private static final String F_KEY_CORD_QTY = "Qty";
    private static final String F_KEY_CORD_UOM = "UOM";
    private static final String F_KEY_CORD_RATE = "Rate";
    private static final String F_KEY_CORD_PRICE = "Price";
    private static final String F_KEY_CORD_MRP = "MRP";
    private static final String F_KEY_CORD_CREATED_ON = "Createdon";


    //CallPlan tabel
    private static final String TABLE_CALL_PLAN = "call_plan";
    // CallPlan table Columns names
    private static final String F_KEY_CP_ID = "serial_no";
    private static final String F_KEY_CP_CALL_PLAN_ID = "call_plan_id";
    private static final String F_KEY_CP_CLIENT_ID = "client_id";
    private static final String F_KEY_CP_LOCATION = "location";
    private static final String F_KEY_CP_CALL_START = "call_start";
    private static final String F_KEY_CP_CALL_END = "call_end";
    private static final String F_KEY_CP_CALL_DURATION = "call_duration";
    private static final String F_KEY_CP_DELIVERY = "delivery";
    private static final String F_KEY_CP_PAYMENT = "payment";
    private static final String F_KEY_CP_RETURN = "return";
    private static final String F_KEY_CP_ORDER = "order_place";


    //CallPlan tabel
    private static final String TABLE_CALL_PLAN_DETAIL = "call_plan_detail";
    // CallPlan table Columns names
    private static final String F_KEY_CPD_ID = "serial_no";
    private static final String F_KEY_CPD_CALL_PLAN_ID = "call_plan_id";
    private static final String F_KEY_CPD_TASK_ID = "task_id";
    private static final String F_KEY_CPD_TASK_NAME = "task_name";
    private static final String F_KEY_CPD_TASK_STATUS = "task_status";
    private static final String F_KEY_CPD_SELFIE = "selfie_image";


    //Chemist order detail table

    private static final String TABLE_CHEMIST_ORDER_DEATILS = "chemist_order_detail";
    // order detail table table Columns names
    private static final String F_KEY_COD_ID = "serial_no";
    private static final String F_KEY_COD_ORDER_NO = "OrderNo";
    private static final String F_KEY_COD_ITEMSR_NO_ = "ItemSrNo";
    private static final String F_KEY_COD_ITEM_NAME = "Item_Name";
    private static final String F_KEY_COD_MRP = "MRP";
    private static final String F_KEY_COD_PACK_SIZE = "Packsize";
    private static final String F_KEY_COD_QTY = "Qty";


    //Chemist order sync
    private static final String TABLE_CHEMIST_ORDER_SYNC = "chemist_order_sync";
    // Chemist order sync table Columns names

    private static final String F_KEY_COS_ID = "serial_no";
    private static final String F_KEY_COS_DOC_ID = "DOC_ID";
    private static final String F_KEY_COS_JSON = "json";
    private static final String F_KEY_COS_SYNCED = "synced";


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {


        String CREATE_TABLE_STOCKIST_WISE_PRODUCTS = "CREATE TABLE " + TABLE_STOCKIST_WISE_PRODUCTS + "("
                + F_KEY_SP_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_SP_ITEMCODE + " TEXT,"
                + F_KEY_SP_STOCKIST_ID + " TEXT,"
                + F_KEY_SP_PRODUCT_ID + " TEXT,"
                + F_KEY_SP_ITEMNAME + " TEXT,"
                + F_KEY_SP_PACKSIZE + " TEXT,"
                + F_KEY_SP_MRP + " TEXT,"
                + F_KEY_SP_RATE + " TEXT ,"
                + F_KEY_SP_STOCK + " TEXT,"
                + F_KEY_SP_TYPE + " TEXT,"
                + F_KEY_SP_MFGCODE + " TEXT,"
                + F_KEY_SP_MFGNAME + " TEXT,"
                + F_KEY_SP_DOSEFORM + " TEXT,"
                + F_KEY_SP_SCHEME + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_STOCKIST_WISE_PRODUCTS);


        String CREATE_TABLE_TABLE_SALESMAN_PRODUCTS = "CREATE TABLE " + TABLE_SALESMAN_PRODUCTS + "("
                + F_KEY_SS_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_SS_ITEMCODE + " TEXT,"
                + F_KEY_SS_STOCKIST_ID + " TEXT,"
                + F_KEY_SS_PRODUCT_ID + " TEXT,"
                + F_KEY_SS_ITEMNAME + " TEXT,"
                + F_KEY_SS_PACKSIZE + " TEXT,"
                + F_KEY_SS_MRP + " TEXT,"
                + F_KEY_SS_RATE + " TEXT ,"
                + F_KEY_SS_STOCK + " TEXT,"
                + F_KEY_SS_TYPE + " TEXT,"
                + F_KEY_SS_MFGCODE + " TEXT,"
                + F_KEY_SS_MFGNAME + " TEXT,"
                + F_KEY_SS_DOSEFORM + " TEXT,"
                + F_KEY_SS_SCHEME + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_TABLE_SALESMAN_PRODUCTS);


        String CREATE_TABLE_CHEMIST_CART = "CREATE TABLE " + TABLE_CHEMIST_CART + "("
                + F_KEY_CC_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_CC_DOC_NO + " TEXT,"
                + F_KEY_CC_STOCKISTID + " TEXT,"
                + F_KEY_CC_ITEM_COUNT + " TEXT,"
                + F_KEY_CC_ORDER_AMOUNT + " TEXT,"
                + F_KEY_CC_ORDER_PLACED + " TEXT,"
                + F_KEY_CC_STATUS + " TEXT,"
                + F_KEY_CC_CREATED_ON + " TEXT,"
                + F_KEY_CC_DOC_DATE + " TEXT,"
                + F_KEY_CC_ORDER_SYNC + " TEXT,"
                + F_KEY_CC_REMARK + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CHEMIST_CART);


        String CREATE_TABLE_CALL_PLAN = "CREATE TABLE " + TABLE_CALL_PLAN + "("
                + F_KEY_CP_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_CP_CALL_PLAN_ID + " TEXT,"
                + F_KEY_CP_CLIENT_ID + " TEXT,"
                + F_KEY_CP_LOCATION + " TEXT,"
                + F_KEY_CP_CALL_START + " TEXT,"
                + F_KEY_CP_CALL_END + " TEXT,"
                + F_KEY_CP_CALL_DURATION + " TEXT,"
                + F_KEY_CP_DELIVERY + " TEXT,"
                + F_KEY_CP_PAYMENT + " TEXT,"
                + F_KEY_CP_ORDER + " TEXT,"
                + F_KEY_CP_RETURN + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CALL_PLAN);


        String CREATE_TABLE_CALL_PLAN_DETAILS = "CREATE TABLE " + TABLE_CALL_PLAN_DETAIL + "("
                + F_KEY_CPD_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_CPD_CALL_PLAN_ID + " TEXT,"
                + F_KEY_CPD_TASK_ID + " TEXT,"
                + F_KEY_CPD_TASK_NAME + " TEXT,"
                + F_KEY_CPD_TASK_STATUS + " TEXT,"
                + F_KEY_CPD_SELFIE + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CALL_PLAN_DETAILS);


        String CREATE_TABLE_CHEMIST_ORDER_DEATILS = "CREATE TABLE " + TABLE_CHEMIST_ORDER_DEATILS + "("
                + F_KEY_COD_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_COD_ORDER_NO + " TEXT,"
                + F_KEY_COD_ITEMSR_NO_ + " TEXT,"
                + F_KEY_COD_ITEM_NAME + " TEXT,"
                + F_KEY_COD_MRP + " TEXT,"
                + F_KEY_COD_PACK_SIZE + " TEXT,"
                + F_KEY_COD_QTY + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CHEMIST_ORDER_DEATILS);


        String CREATE_TABLE_CHEMIST_ORder_sync = "CREATE TABLE " + TABLE_CHEMIST_ORDER_SYNC + "("
                + F_KEY_COS_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_COS_DOC_ID + " TEXT,"
                + F_KEY_COS_JSON + " TEXT,"
                + F_KEY_COS_SYNCED + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CHEMIST_ORder_sync);


        String CREATE_TABLE_CHEMIST_ORDER = "CREATE TABLE " + TABLE_CHEMIST_ORDER + "("
                + F_KEY_COR_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_COR_DOC_NO + " TEXT,"
                + F_KEY_COR_DOC_DATE + " TEXT,"
                + F_KEY_COR_STOCKIST_ID + " TEXT,"
                + F_KEY_COR_ITEMS + " TEXT,"
                + F_KEY_COR_REMARK + " TEXT,"
                + F_KEY_COR_ORDER_AMOUNT + " TEXT,"
                + F_KEY_COR_ORDER_STATUS + " TEXT,"
                + F_KEY_COR_CREATEDON + " TEXT,"
                + F_KEY_COR_CREATEBY + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CHEMIST_ORDER);


        String CREATE_TABLE_LEGEND_MASTER = "CREATE TABLE " + TABLE_LEGEND_MASTER + "("
                + F_KEY_LM_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_LM_STOCKIST_ID + " TEXT,"
                + F_KEY_LM_LEGENDNAME + " TEXT,"
                + F_KEY_LM_COLOR_CODE + " TEXT,"
                + F_KEY_LM_START_RANGE + " TEXT,"
                + F_KEY_LM_END_RANGE + " TEXT,"
                + F_KEY_LM_STOCKLEGENDID + " TEXT,"
                + F_KEY_LM_LEGENDMODE + " TEXT"
                + ")";


        db.execSQL(CREATE_TABLE_LEGEND_MASTER);


        String CREATE_TABLE_CHEMIST_ORDER_DETAIL = "CREATE TABLE " + TABLE_CHEMIST_CART_DETAIL + "("
                + F_KEY_CORD_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT,"
                + F_KEY_CORD_DOC_NO + " TEXT,"
                + F_KEY_CORD_DOC_ITEM_NO + " TEXT,"
                + F_KEY_CORD_PRODUCT_ID + " TEXT,"
                + F_KEY_CORD_QTY + " TEXT,"
                + F_KEY_CORD_UOM + " TEXT,"
                + F_KEY_CORD_RATE + " TEXT,"
                + F_KEY_CORD_PRICE + " TEXT,"
                + F_KEY_CORD_MRP + " TEXT,"
                + F_KEY_CORD_CREATED_ON + " TEXT " + ")";


        db.execSQL(CREATE_TABLE_CHEMIST_ORDER_DETAIL);

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void inset_into_stockist_wise_products(String product_id, String item_code, String stockist_id, String item_name, String pack_size, String mrp,
                                                  String rate, String stock, String type, String mfg_code, String mfg_name, String dosage_form,
                                                  String scheme) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.setLockingEnabled(false);

        ContentValues values = new ContentValues();
        values.put(F_KEY_SP_PRODUCT_ID, product_id);
        values.put(F_KEY_SP_ITEMCODE, item_code);
        values.put(F_KEY_SP_STOCKIST_ID, stockist_id);
        values.put(F_KEY_SP_ITEMNAME, item_name);
        values.put(F_KEY_SP_PACKSIZE, pack_size);
        values.put(F_KEY_SP_MRP, mrp);
        values.put(F_KEY_SP_RATE, rate);
        values.put(F_KEY_SP_STOCK, stock);
        values.put(F_KEY_SP_TYPE, type);
        values.put(F_KEY_SP_MFGCODE, mfg_code);
        values.put(F_KEY_SP_MFGNAME, mfg_name);
        values.put(F_KEY_SP_DOSEFORM, dosage_form);
        values.put(F_KEY_SP_SCHEME, scheme);
        // Inserting Row
        long id = db.insert(TABLE_STOCKIST_WISE_PRODUCTS, null, values);
        // db.close(); //
    }


    public void inset_into_salesman_products(String product_id, String item_code, String item_name, String pack_size, String mrp,
                                             String rate, String stock, String type, String mfg_code, String mfg_name, String dosage_form,
                                             String scheme) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.setLockingEnabled(false);

        ContentValues values = new ContentValues();
        values.put(F_KEY_SS_PRODUCT_ID, product_id);
        values.put(F_KEY_SS_ITEMCODE, item_code);
        values.put(F_KEY_SS_ITEMNAME, item_name);
        values.put(F_KEY_SS_PACKSIZE, pack_size);
        values.put(F_KEY_SS_MRP, mrp);
        values.put(F_KEY_SS_RATE, rate);
        values.put(F_KEY_SS_STOCK, stock);
        values.put(F_KEY_SS_TYPE, type);
        values.put(F_KEY_SS_MFGCODE, mfg_code);
        values.put(F_KEY_SS_MFGNAME, mfg_name);
        values.put(F_KEY_SS_DOSEFORM, dosage_form);
        values.put(F_KEY_SS_SCHEME, scheme);
        // Inserting Row
        long id = db.insert(TABLE_SALESMAN_PRODUCTS, null, values);
        // db.close(); //
    }


    public void insert_into_stockist_legend(String Stockist_id, String legend_name, String color_code,
                                            String start_range,
                                            String end_range, String stockist_legend_id,
                                            String legend_mode) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(F_KEY_LM_STOCKIST_ID, Stockist_id);
        values.put(F_KEY_LM_LEGENDNAME, legend_name);
        values.put(F_KEY_LM_COLOR_CODE, color_code);
        values.put(F_KEY_LM_START_RANGE, start_range);
        values.put(F_KEY_LM_END_RANGE, end_range);
        values.put(F_KEY_LM_STOCKLEGENDID, stockist_legend_id);
        values.put(F_KEY_LM_LEGENDMODE, legend_mode);

        long id = db.insert(TABLE_LEGEND_MASTER, null, values);
        // db.close(); //
    }


    public void insert_into_chemist_cart(String Doc_id, String stockist_id, String Order_amount,
                                         String Doc_date,
                                         Integer itemcount, String remark,
                                         Integer order_placed, Integer order_sync) {
        SQLiteDatabase db = this.getWritableDatabase();


        String countQuery = "SELECT * FROM " + TABLE_CHEMIST_CART + " where " + F_KEY_CC_DOC_NO + "='" + Doc_id + "'";

        int previous_itemcount = 0;
        float previous_price = 0;
        // Cursor c = db.rawQuery(countQuery, null);
        ContentValues values = new ContentValues();

       /* if (c.getCount() > 0) {
            c.moveToFirst();


            try {


                previous_itemcount = Integer.parseInt(c.getString(c.getColumnIndex("Items")));
                previous_price = Float.parseFloat(c.getString(c.getColumnIndex(F_KEY_CORD_PRICE)));
                Log.v("OLDItem", String.valueOf(previous_itemcount));
                values.put(F_KEY_CC_ITEM_COUNT, itemcount + previous_itemcount);
                values.put(F_KEY_CC_ORDER_AMOUNT, String.valueOf(Float.parseFloat(Order_amount) + previous_price));
                db.update(TABLE_CHEMIST_CART, values, "DOC_NO='" + Doc_id + "'", null);
                c.close();
                return;

            } catch (NumberFormatException nfe) {

                c.close();
            }

        }

        c.close();*/


        values.put(F_KEY_CC_DOC_NO, Doc_id);
        values.put(F_KEY_CC_STOCKISTID, stockist_id);
        values.put(F_KEY_CC_ORDER_AMOUNT, Order_amount);
        values.put(F_KEY_CC_DOC_DATE, Doc_date);
        values.put(F_KEY_CC_CREATED_ON, Doc_date);
        values.put(F_KEY_CC_STATUS, 0);
        values.put(F_KEY_CC_ITEM_COUNT, itemcount);
        values.put(F_KEY_CC_ORDER_PLACED, order_placed);
        values.put(F_KEY_CC_ORDER_SYNC, order_sync);
        values.put(F_KEY_CC_REMARK, remark);

        long id = db.insert(TABLE_CHEMIST_CART, null, values);
        // db.close(); //
    }


    public void insert_into_chemist_cart_details(String Doc_id, String item_number, String product_id,
                                                 Integer Quantity, String uom, String rate,
                                                 String price, String mrp,
                                                 String date) {
        SQLiteDatabase db = this.getWritableDatabase();


      /*  String countQuery = "SELECT  * FROM " + TABLE_CHEMIST_CART_DETAIL + " where " + F_KEY_CORD_PRODUCT_ID + "=?";


        int previous_itemcount = 0;
        float previous_price = 0;
        Cursor c = db.rawQuery(countQuery, new String[]{product_id});*/
        ContentValues values = new ContentValues();


       /* if (c.getCount() > 0) {
            c.moveToFirst();
            try {


                previous_itemcount = Integer.parseInt(c.getString(c.getColumnIndex(F_KEY_CORD_QTY)));
                previous_price = Float.parseFloat(c.getString(c.getColumnIndex(F_KEY_CORD_PRICE)));
                Log.v("OLDItem", String.valueOf(previous_itemcount));
                values.put(F_KEY_CORD_QTY, Quantity + previous_itemcount);
                values.put(F_KEY_CORD_PRICE, String.valueOf(Float.parseFloat(price) + previous_price));
                db.update(TABLE_CHEMIST_CART_DETAIL, values, "Product_ID='" + product_id + "'", null);
                c.close();
                return;


            } catch (NumberFormatException nfe) {

                c.close();


            }
        }

        c.close();
*/

        values.put(F_KEY_CORD_DOC_NO, Doc_id);
        values.put(F_KEY_CORD_DOC_ITEM_NO, item_number);
        values.put(F_KEY_CORD_PRODUCT_ID, product_id);
        values.put(F_KEY_CORD_QTY, Quantity);
        values.put(F_KEY_CORD_UOM, uom);
        values.put(F_KEY_CORD_RATE, rate);
        values.put(F_KEY_CORD_PRICE, price);
        values.put(F_KEY_CORD_MRP, mrp);
        values.put(F_KEY_CORD_CREATED_ON, date);

        long id = db.insert(TABLE_CHEMIST_CART_DETAIL, null, values);
        // db.close(); //
    }


    public void insert_into_chemist_order_sync(String doc_id, String json, Integer Synced) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(F_KEY_COS_DOC_ID, doc_id);
        values.put(F_KEY_COS_JSON, json);
        values.put(F_KEY_COS_SYNCED, Synced);

        long id = db.insert(TABLE_CHEMIST_ORDER_SYNC, null, values);
        //db.close(); //
    }


    public void update_into_chemist_cart(String Doc_id, Integer item_count, String amount) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(F_KEY_CC_ITEM_COUNT, item_count);
        values.put(F_KEY_CC_ORDER_AMOUNT, amount);


        long id = db.update(TABLE_CHEMIST_CART, values, "DOC_NO='" + Doc_id + "'", null);
        //db.close(); //
    }


    public Cursor get_chemist_cart_data(String Doc_id) {

        String buildSQL = "select distinct ccd.DOC_NO,  ccd.*,sp.* FROM " + TABLE_STOCKIST_WISE_PRODUCTS +
                "  sp, " + TABLE_CHEMIST_CART_DETAIL + " ccd " +
                " where   sp.Product_ID=ccd.Product_ID  and ccd.DOC_NO='" + Doc_id + "' order by ccd.DOC_NO";

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }

    public Cursor get_saleman_cart_data(String Doc_id) {

        String buildSQL = "select distinct ccd.DOC_NO,  ccd.*,sp.* FROM " + TABLE_SALESMAN_PRODUCTS +
                "  sp, " + TABLE_CHEMIST_CART_DETAIL + " ccd " +
                " where   sp.Product_ID=ccd.Product_ID  and ccd.DOC_NO='" + Doc_id + "' order by ccd.DOC_NO";

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }


    public Boolean check_saleman_product_availabity(String p_id) {

        Boolean p_availble = false;

        String buildSQL = "select * FROM " + TABLE_SALESMAN_PRODUCTS + " where Itemcode='" + p_id + "'";

        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.rawQuery(buildSQL, null);

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                p_availble = true;
            }
            cursor.close();

            // return row count

        }

        return p_availble;


    }


    public Cursor get_chemist_cart(String Stockist_id) {

        String buildSQL = "select * FROM " + TABLE_CHEMIST_CART + " where Stockist_Client_id=" + Stockist_id;
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }

    public Cursor get_legend_data(String Stockist_id) {

        String buildSQL = "select * FROM " + TABLE_LEGEND_MASTER + " where stockistId=" + Stockist_id;
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }


    public Cursor delete_legend_data(String Stockist_id) {

        String buildSQL = "delete FROM " + TABLE_LEGEND_MASTER + " where stockistId=" + Stockist_id;
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }


    public Cursor get_chemist_cart_detail(String DocId) {

        String buildSQL = "select * FROM " + TABLE_CHEMIST_CART_DETAIL + " where DOC_NO='" + DocId + "'";
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }


    public Cursor get_chemist_order_details(String Order_id) {

        String buildSQL = "select  * FROM " + TABLE_CHEMIST_ORDER_DEATILS + "    where OrderNo=" + Order_id;

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(buildSQL, null);
    }

    public int check_cart_id(String cart_id) {

        String buildSQL = "select * FROM " + TABLE_CHEMIST_CART + "  where CartId='" + cart_id + "'";

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(buildSQL, null);
        int rowCount = cursor.getCount();
        //db.close();
        cursor.close();

        // return row count
        return rowCount;
    }


    // insert data using transaction and prepared statement
    public void insertInto_call_plan(String call_id, String client_id, String Location, int call_started, String call_duration, int delivery,
                                     int payment, int returns, int order) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransactionNonExclusive();
        try {


            ContentValues values = new ContentValues();
            values.put(F_KEY_CP_CALL_PLAN_ID, call_id); // Name
            values.put(F_KEY_CP_CLIENT_ID, client_id); // Name
            values.put(F_KEY_CP_LOCATION, Location); // Name
            values.put(F_KEY_CP_CALL_START, call_started); // Name
            values.put(F_KEY_CP_CALL_DURATION, call_duration); // Name
            values.put(F_KEY_CP_DELIVERY, delivery); // Name
            values.put(F_KEY_CP_PAYMENT, payment); // Name
            values.put(F_KEY_CP_RETURN, returns); // Name
            values.put(F_KEY_CP_ORDER, order); // Name

            db.insertOrThrow(TABLE_CALL_PLAN, null, values);

            db.setTransactionSuccessful();
        } catch (Exception e) {

        } finally {
            db.endTransaction();
        }

    }


    public void insertInto_Chemist_order_details(String Order_no, String item_srno, String item_name, Double MRP, Integer packsize, Integer Qty) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransactionNonExclusive();
        try {


            ContentValues values = new ContentValues();
            values.put(F_KEY_COD_ORDER_NO, Order_no); // Name
            values.put(F_KEY_COD_ITEMSR_NO_, item_srno); // Name
            values.put(F_KEY_COD_MRP, MRP); // Name
            values.put(F_KEY_COD_PACK_SIZE, packsize); // Name
            values.put(F_KEY_COD_QTY, Qty); // Name
            values.put(F_KEY_COD_ITEM_NAME, item_name); // Name

            db.insertOrThrow(TABLE_CHEMIST_ORDER_DEATILS, null, values);

            db.setTransactionSuccessful();
        } catch (Exception e) {

        } finally {
            db.endTransaction();
        }

    }

    public int check_stockist_data(String Stockist_id) {
        String countQuery = "SELECT  * FROM " + TABLE_STOCKIST_WISE_PRODUCTS + " where stockist_id=" + Stockist_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();

        cursor.close();

        // return row count
        return rowCount;
    }

    public Float get_total_order_amount(String Doc_id) {

        Float amount = 0f;
        String countQuery = "SELECT  * FROM " + TABLE_CHEMIST_CART + " where DOC_NO='" + Doc_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                amount = cursor.getFloat(cursor.getColumnIndex("Amount"));
            }
            cursor.close();

            // return row count

        }
        return amount;
    }


    public Integer get_total_order_item_count(String Doc_id) {

        Integer count = 0;
        String countQuery = "SELECT  * FROM " + TABLE_CHEMIST_CART;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
     /*   if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                count = cursor.getInt(cursor.getColumnIndex(F_KEY_CC_DOC_NO));
            }*/


        count = cursor.getCount();
        cursor.close();

        // return row count

        // }
        return count;
    }


    public Cursor get_stockist_data(String Stockist_id) {
        String countQuery = "SELECT  * FROM " + TABLE_STOCKIST_WISE_PRODUCTS + " where stockist_id=" + Stockist_id + " order by Itemname";
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(countQuery, null);
    }

    public Cursor get_stockist_inventory() {
        String countQuery = "SELECT  * FROM " + TABLE_SALESMAN_PRODUCTS;
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(countQuery, null);
    }


    public Cursor get_order_json() {
        String countQuery = "SELECT  * FROM " + TABLE_CHEMIST_ORDER_SYNC;
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(countQuery, null);
    }

    public Cursor get_order_json_on_doc_id(String doc_id) {
        String countQuery = "SELECT  * FROM " + TABLE_CHEMIST_ORDER_SYNC + " where DOC_ID='" + doc_id + "'";
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(countQuery, null);
    }

    public void delete_chemist_order_json(String p_id) {


        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            int i = db.delete(TABLE_CHEMIST_ORDER_SYNC, "DOC_ID = '" + p_id + "'", null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }

    public void delete_chemist_Cart(String doc_id) {


        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            int i = db.delete(TABLE_CHEMIST_CART, "DOC_NO = '" + doc_id + "'", null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }

    public void delete_chemist_Cart_Details(String doc_id) {


        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            int i = db.delete(TABLE_CHEMIST_CART_DETAIL, "DOC_NO = '" + doc_id + "'", null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }

    public void delete_product_from_cart_chemist_Cart_Details(String doc_id, String itemno) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            int i = db.delete(TABLE_CHEMIST_CART_DETAIL, "DOC_NO = '" + doc_id + "'   and Doc_item_No='" + itemno + "'", null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }


    public void delete_product_from_cart_chemist_Cart_Details(String createdOn) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            int i = db.delete(TABLE_CHEMIST_CART_DETAIL, " Createdon = '" + createdOn + "'  ", null);
            // int i=db.delete(TABLE_CHEMIST_CART_DETAIL, "DOC_NO = '" + doc_id+"'", null);

            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }

    public void delete_stockist_inventory() {


        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            int i = db.delete(TABLE_SALESMAN_PRODUCTS, "", null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            db.endTransaction();
        }
    }

    public Cursor get_stockist_id_on_medicine_name(String medicine_name) {
        String countQuery = "SELECT  distinct Stockist_id FROM " + TABLE_STOCKIST_WISE_PRODUCTS + " where Itemname like '%" + medicine_name + "%'";
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(countQuery, null);
    }

  /*  public Cursor generate_order_data()
    {

    }*/

}