package com.synergy.ordergenie.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.synergy.ordergenie.R;
import com.synergy.ordergenie.adapter.MakePaymentListAdapter;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.model.m_pendingbills;
import com.synergy.ordergenie.utils.MakeWebRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.synergy.ordergenie.utils.ConstData.user_info.CHEMIST_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_NAME;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class SalesmanMakePayment extends AppCompatActivity implements MakeWebRequest.OnResponseSuccess, MakePaymentListAdapter.SelectedListItem {
    private MakePaymentListAdapter paymentListAdapter;
    private RecyclerView rec_payment;
    private Boolean IsCallPlanTask;
    SharedPreferences pref;
    List<m_pendingbills> posts;
    //List<List<m_pendingbills>> selectedFinalList=new ArrayList<List<m_pendingbills>>();
    List<m_pendingbills> selectedFinalListt;
    List<m_pendingbills> selectedFinalListt1=new ArrayList<m_pendingbills>();
    private String Client_id, Stockist_id, chemist_Name, getChemistId;
    private TextView empty_view;
    private TextView txt_makepayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salesman_make_payment);


        selectedFinalListt = new ArrayList<m_pendingbills>();

        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);
        empty_view = (TextView) findViewById(R.id.empty_view);
        txt_makepayment = (TextView) findViewById(R.id.complete_payment);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rec_payment = (RecyclerView) findViewById(R.id.recycler_payment);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rec_payment.getContext(),
                DividerItemDecoration.VERTICAL);
        rec_payment.addItemDecoration(dividerItemDecoration);
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        IsCallPlanTask = getIntent().getBooleanExtra("call_plan_task", false);
        chemist_Name = getIntent().getStringExtra(CLIENT_NAME);
        setTitle(chemist_Name);
        Client_id = pref.getString(CLIENT_ID, "0");
        Stockist_id = pref.getString(USER_ID, "0");
        getChemistId = getIntent().getStringExtra(CHEMIST_ID);
        txt_makepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("selectedFinalListt1", selectedFinalListt.toString());
                Intent i = new Intent(getApplicationContext(), SelectedpaymentList.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("selected_items", (Serializable) selectedFinalListt);
                i.putExtras(bundle);

               // i.putExtra("selected_items", String.valueOf(selectedFinalListt));


              //  i.putParcelableArrayListExtra("selected_item", (ArrayList<? extends Parcelable>) selectedFinalListt);
                startActivity(i);

            }
        });


    }


    @Override
    public void onResume() {

        if (IsCallPlanTask) {
            Log.d("id", getIntent().getStringExtra("chemist_id") + "--" + Client_id);
            MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST,
                    AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST + "[" + getIntent().getStringExtra("chemist_id") + "," + Client_id + "]", this, true);

        } else {
            /*MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST,
                    AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST +"["+Stockist_id+","+Chemist_id+"]" , this, true);
*/
            MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST,
                    AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST + "[" + getChemistId + "," + Client_id + "]", this, true);
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_make_payment, menu);
        return true;
    }


    public void fill_payment_list(final List<m_pendingbills> posts_s) {

        if (posts.size() > 0) {
            empty_view.setVisibility(View.GONE);
        } else {
            empty_view.setVisibility(View.VISIBLE);
        }

        Log.d("size", String.valueOf(posts_s.size()));
        paymentListAdapter = new MakePaymentListAdapter(posts_s, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rec_payment.setLayoutManager(mLayoutManager);
        rec_payment.setItemAnimator(new DefaultItemAnimator());
        rec_payment.setAdapter(paymentListAdapter);
    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {

        if (response != null) {
            try {

                if (f_name.equals(AppConfig.GET_STOCKIST_INDIVIDUAL_PENDINGLIST)) {

                    String jsondata = response.toString();
                    if (!jsondata.isEmpty()) {
                        GsonBuilder builder = new GsonBuilder();
                        Gson mGson = builder.create();
                        posts = new ArrayList<m_pendingbills>();
                        posts = Arrays.asList(mGson.fromJson(jsondata, m_pendingbills[].class));
                        fill_payment_list(posts);
                    }
                }

            } catch (Exception e) {
                e.toString();
            }
        }

    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {

    }

    @Override
    public void setValues(List<m_pendingbills> al) {
        Log.d("itemlist", al.toString());
       // selectedFinalListt = new ArrayList<m_pendingbills>(al);
       // selectedFinalListt1.addAll(selectedFinalListt);
        selectedFinalListt.addAll(al);
        Log.d("selectedFinalListt", selectedFinalListt.toString());

        // Log.d("selectedFinalList",selectedFinalList.toString());
    }
}
