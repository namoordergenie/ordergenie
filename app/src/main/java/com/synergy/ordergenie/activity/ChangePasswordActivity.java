package com.synergy.ordergenie.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;
import com.synergy.ordergenie.utils.ConstData;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.OGtoast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;


/**
 * Created by AS Infotech on 28-10-2017.
 */

public class ChangePasswordActivity extends AppCompatActivity implements MakeWebRequest.OnResponseSuccess {
    EditText oldpass, newpass, confirmpass;
    String _oldpass, _newpass, _confirmpass;
    Boolean dummypass = false;
    Button Save;
    AppController globalVariable;
    private static final String TAG = "ChangePasswordActivity";
    @BindView(R.id.toolbar)
    Toolbar _toolbar;
    //  String change_token = "changepassword";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this ,R.color.colorPrimaryDark));
        }

       // if (getIntent().getBooleanExtra(CHEMIST_IS_LAST_10_ORDER, false)) {
       //     actionBar.setTitle("Change Password");


        oldpass = (EditText) findViewById(R.id.oldpassword);
        confirmpass = (EditText) findViewById(R.id.confirm_password);
        newpass = (EditText) findViewById(R.id.newpassword);
        Save = (Button) findViewById(R.id.btSubmit);
        globalVariable = (AppController) getApplicationContext();
        setSupportActionBar(_toolbar);

        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _oldpass = oldpass.getText().toString().trim();
                _newpass = newpass.getText().toString().trim();
                _confirmpass = confirmpass.getText().toString().trim();

                // changepasswordprocess();
                if (validate()) {
                    check_passwordchange(_oldpass, _newpass, _confirmpass);
                }
//                Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
//                startActivity(intent);
//                if(_oldpass.length()==""||){}
//                else{
//
//
//                }
//
//


            }
        });



    }

    //    public void changepasswordprocess() {
//            //       Log.d(TAG, "Login");
//
//            if (!validate()) {
//                onLoginFailed();
//                return;
//            }
//
//
//     //   check_passwordchange(_oldpass, _newpass,_confirmpass);
//        // TODO: Implement your own authentication logic here.
//
//
//    }
    public void onLoginFailed() {
        OGtoast.OGtoast("Please try again", getBaseContext());

        Save.setEnabled(true);
    }
    public boolean validate() {
        boolean valid = true;

        _oldpass = oldpass.getText().toString();
        _confirmpass = confirmpass.getText().toString();
        _newpass = newpass.getText().toString();

        if (_oldpass.isEmpty()) {
            oldpass.setError("enter a valid password");
            valid = false;
        } else {
            oldpass.setError(null);
        }

        if (_newpass.isEmpty() || _newpass.length() < 4 ) {
            //***     if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            newpass.setError("password strength should be greater than 4");
            valid = false;
        } else {
            newpass.setError(null);
        }
        if (_confirmpass.isEmpty() || _confirmpass.length() < 4) {
            //***     if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            confirmpass.setError("password strength should be greater than 4");
            valid = false;
        } else {
            confirmpass.setError(null);
        }
        if (_confirmpass.equals(_newpass)) {
            //***     if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            confirmpass.setError(null);

            //  valid = true;
        } else {
            confirmpass.setError("Please enter valid password");
            valid = false;
        }

        return valid;
    }

    private void check_passwordchange(String _oldpass, String _newpass, String _confirmpass) {



        if (_confirmpass.equals(_newpass)&&_confirmpass.length()>4) {

            try {

                JSONObject jsonParams = new JSONObject();
                Map<String, String> j_obj = new HashMap<>();

                j_obj.put("oldPassword", _oldpass);
                j_obj.put("newPassword", _newpass);
                j_obj.put("confPassword", _confirmpass);
                j_obj.put("isDummyPwd", dummypass.toString());
        //        Log.e("changepassword", String.valueOf(j_obj));

               // Log.e("jsonParams", jsonParams.toString());
                MakeWebRequest.put_request(this, AppConfig.checkpasswordchange, j_obj);
//                Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
//                startActivity(intent);
                //globalVariable.setToken(null);
                //  MakeWebRequest.MakeWebRequest("put", "Login", AppConfig.checkpasswordchange, jsonParams, this, true);
            } catch (Exception e) {

            }
            // }
        }else{
            Toast.makeText(getApplicationContext(),"Please submit valid password of strength greater than 4",Toast.LENGTH_SHORT).show();


        }
    }

    @Override

    public void onSuccess_json_object(String f_name, JSONObject response) {

        try {
            if (response != null) {
                //Log.d("print_pass_status11",response.toString());
                Toast.makeText(getApplicationContext(),"Password changed successfully.",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                }
//                Log.d("print_pass_status11",response.toString());
//                Toast.makeText(getApplicationContext(),"Password changed successfully."+response.toString(),Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
//                startActivity(intent);

           else if(response == null||response.equals("")) {

                //Log.d("print_pass_status11","dunno");
                if (isNetworkConnectionAvailable()) {
                    oldpass.setText("");
                    newpass.setText("");
                    confirmpass.setText("");
                    Toast.makeText(getApplicationContext(),"Failed to change password",Toast.LENGTH_SHORT).show();

                   //  OGtoast.OGtoast("Failed to change password", getBaseContext());
                }
                else
                {
                    oldpass.setText("");
                    newpass.setText("");
                    confirmpass.setText("");
                    Toast.makeText(getApplicationContext(),"Please check your Internet connection",Toast.LENGTH_SHORT).show();
                   // OGtoast.OGtoast("Please check your Internet connection", getBaseContext());
                }

            }
            else{

                oldpass.setText("");
                newpass.setText("");
                confirmpass.setText("");
                Toast.makeText(getApplicationContext(),"Old password is not valid. Please try again, later.",Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            oldpass.setText("");
            newpass.setText("");
            confirmpass.setText("");
        }
    }


    public boolean isNetworkConnectionAvailable(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            //Log.d("Network", "Connected");
            return true;
        }
        else{
            // checkNetworkConnection();
            //Log.d("Network","Not Connected");
            return false;
        }
    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {
        try {
            if (response == null) {
              //  Log.d("print_pass_status","null");
                oldpass.setText("");
                newpass.setText("");
                confirmpass.setText("");
                Toast.makeText(getApplicationContext(),"Old password is not valid. Please try again, later.",Toast.LENGTH_LONG).show();
                //wrong
             //   Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
             //   startActivity(intent);

            }
// else {
//
//                Log.d("print_pass_status","not null");
//                if (isNetworkConnectionAvailable()) {
//                    Toast.makeText(getApplicationContext(),"response not null ",Toast.LENGTH_SHORT).show();
//                   // OGtoast.OGtoast("Failed to change password", getBaseContext());
//                }
//                else
//                {
//
//                    Toast.makeText(getApplicationContext(),"response ",Toast.LENGTH_SHORT).show();
//                   // OGtoast.OGtoast("Please check your Internet connection", getBaseContext());
//                }
//
//            }
//
//
        } catch (Exception e) {
            oldpass.setText("");
            newpass.setText("");
            confirmpass.setText("");
            Toast.makeText(getApplicationContext(),"Old password is not valid. Please try again, later.",Toast.LENGTH_LONG).show();
        }
    }

    //*************
//    @Override
//    public void onSuccess_json_array(String f_name, JSONArray response) {
//
//        //Log.d("Login", f_name);
//        if (response != null) {
//
//            if (f_name.equals("GetMe")) {
//                Toast.makeText(getApplicationContext(),"dono", Toast.LENGTH_SHORT).show();
//               // showmainscreen(response);
//            } else {
//                OGtoast.OGtoast("Unable to contact the server", getBaseContext());
//            }
//        }
//    }
    //*************

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        if (globalVariable.getFromHomeActivity())
        {
            //Log.d("print_pass_status","Home");
            Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
            startActivity(intent);
            globalVariable.setFromHomeActivity(false);
        }
        else {
       //     Log.d("print_pass_status","not Home");
            Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }
//    private void showmainscreen(JSONArray response) {
//
//        Log.d("print_login", String.valueOf(response));
//        Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
//
//            startActivity(intent);
//
//
//
//    }
}
