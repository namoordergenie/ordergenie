package com.synergy.ordergenie.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.synergy.ordergenie.BR;
import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;
import com.synergy.ordergenie.model.m_customerlist;
import com.synergy.ordergenie.model.m_stokiest_distributor_list;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.get_current_location;

import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_NAME;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ROLE;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

/**
 * Created by prakash on 08/07/16.
 */
public class CustomerlistActivity extends AppCompatActivity implements MakeWebRequest.OnResponseSuccess {

    private static final String CHEMIST_ID = "Chemist_id";
    private Snackbar snackbar;
    private BottomSheetBehavior behavior;
    private SearchView searchView;
    private Menu oMenu;
    private Integer selected_customer_id;
    RecyclerView.Adapter<BindingViewHolder> adapter;
    SharedPreferences pref;
    public static final String CHEMIST_STOCKIST_NAME = "chemist_stockist_name";
    private String Selected_chemist_id;

    public static String selected_Ten_orders;
    private String Chemist_id;
    private String Selected_client_name;
    private String User_id, Stockist_id, Role;
    public static final String SELECTED_CHEMIST_ID = "selected_chemist_id";
    private List<m_customerlist> mModels = new ArrayList<>();


    private List<m_stokiest_distributor_list> mStokiestmodels = new ArrayList<>();
    AppController globalVariable;
    @BindView(R.id.rv_datalist)
    RecyclerView rvCustomerlist;

    @BindView(R.id.bottom_sheet)
    View _bottomSheet;

    @Nullable
    @BindView(R.id.fab)
    FloatingActionButton _fab;

    @BindView(R.id.toolbar)
    Toolbar _toolbar;

    @BindView(R.id.sp_customer_type)
    Spinner _sp_customer_type;

    @Nullable
    @BindView(R.id.txt_cust_count)
    TextView txt_cust_count;

    @BindView(R.id.btm_text_cust_name)
    TextView btm_text_cust_name;


    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_cus_code)
    TextView txt_cus_code;
    @BindView(R.id.txt_cus_address)
    TextView txt_cus_address;
    @BindView(R.id.txt_cus_email)
    TextView txt_cus_email;
    @BindView(R.id.txt_cus_phone)
    TextView txt_cus_phone;
    @BindView(R.id.txt_cus_outstanding)
    TextView txt_cus_outstanding;


    @BindView(R.id.main_coordinate)
    CoordinatorLayout _main_coordinate;


    @OnClick(R.id.main_coordinate)
    void onclickcoordinate(View view) {
        if (snackbar != null) {
            snackbar.dismiss();
            if (_fab.getVisibility() == View.GONE) {
                _fab.setVisibility(View.VISIBLE);
            }
        }
    }


    @OnClick(R.id.fab)
    void onclickfab(View view) {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            findViewById(R.id.list_options).setVisibility(View.VISIBLE);
            findViewById(R.id.profile_details).setVisibility(View.GONE);
            _bottomSheet.invalidate();
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            //create_snack_bar(view);
            new get_current_location(CustomerlistActivity.this);

        }
    }

    @OnClick(R.id.lnr_view_profile)
    void onclickprofile(View view) {

        findViewById(R.id.list_options).setVisibility(View.GONE);
        findViewById(R.id.profile_details).setVisibility(View.VISIBLE);


    }

    @OnClick(R.id.lnr_view_pending_bills)
    void pendingbills(View view) {

        Show_pending_bills();

    }

    @OnClick(R.id.lnr_view_all_orders)
    void allorders(View view) {

        Show_Orders();

    }

    @OnClick(R.id.lnr_view_sales_return)
    void salesreturns(View view) {

        Show_sales_returns();

    }

    @OnClick(R.id.lnr_create_new_order)
    void createorder(View view) {
        Create_new_order();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_bar_customerlist);

        ButterKnife.bind(this);
        setSupportActionBar(_toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle("CustomerList");
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        globalVariable = (AppController) getApplicationContext();
        Chemist_id = pref.getString(CHEMIST_ID, "0");
        User_id = pref.getString(USER_ID, "0");
        Stockist_id = pref.getString(CLIENT_ID, "0");
        Role = pref.getString(CLIENT_ROLE, "0");
        //     Log.e("Role", Role);
        //     Log.e("Stockist_id", Stockist_id + User_id);
        behavior = BottomSheetBehavior.from(_bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) _fab.getLayoutParams();
                    p.setAnchorId(R.id.lnr_bottom);
                    _fab.setLayoutParams(p);
                    findViewById(R.id.list_options).setVisibility(View.VISIBLE);
                    findViewById(R.id.profile_details).setVisibility(View.GONE);
                    _bottomSheet.invalidate();
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {
            }
        });
        if (Role.equals("stockist")) {
            MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_CUSTOMERLIST_STOCKISTLOGIN,
                    AppConfig.GET_STOCKIST_CUSTOMERLIST_STOCKISTLOGIN + Stockist_id, this, true);
        } else {
            MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_CUSTOMERLIST,
                    AppConfig.GET_STOCKIST_CUSTOMERLIST + "[" + Stockist_id + "," + User_id + "]", this, true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_customerlist, menu);
        oMenu = menu;
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                filter_on_text(newText);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                _toolbar.findViewById(R.id.sp_customer_type).setVisibility(View.VISIBLE);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _toolbar.findViewById(R.id.sp_customer_type).setVisibility(View.GONE);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private class BindingViewHolder extends RecyclerView.ViewHolder {
        public BindingViewHolder(View itemView) {
            super(itemView);
        }
        public ViewDataBinding getBinding() {
            return DataBindingUtil.getBinding(itemView);
        }
    }

    private void update_recycleview(final List<m_customerlist> post) {
        txt_cust_count.setText(post.size() + " Customers");
        adapter = new RecyclerView.Adapter<BindingViewHolder>() {
            @Override
            public BindingViewHolder onCreateViewHolder(ViewGroup parent,
                                                        final int viewType) {
                LayoutInflater inflater = LayoutInflater.from(CustomerlistActivity.this);
                ViewDataBinding binding = DataBindingUtil
                        .inflate(inflater, R.layout.fragement_customerlist_items, parent, false);

                return new BindingViewHolder(binding.getRoot());
            }

            @Override
            public void onBindViewHolder(BindingViewHolder holder, final int position) {
                m_customerlist omline_customerlist = post.get(position);
                holder.getBinding().setVariable(BR.v_customerlist, omline_customerlist);
                holder.getBinding().executePendingBindings();
                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        show_bottom_sheet(v, post.get(position).getCustomerName());
                        Selected_chemist_id = post.get(position).getChemist_id();
                        selected_Ten_orders = Selected_chemist_id;
                        Selected_client_name = post.get(position).getCustomerName();
                        txt_name.setText(post.get(position).getCustomerName());
                        txt_cus_code.setText(post.get(position).getCust_Code());
                        txt_cus_address.setText(post.get(position).getLocation());
                        txt_cus_email.setText(post.get(position).getEmail_id());
                        txt_cus_phone.setText(post.get(position).getMobile());
                        if (post.get(position).getOutstanding_Bill() != null) {
                            txt_cus_outstanding.setText(post.get(position).getOutstanding_Bill());
                        }
                    }
                });

            }

            @Override
            public int getItemCount() {
                return post.size();
            }
        };
        rvCustomerlist.setLayoutManager(new LinearLayoutManager(CustomerlistActivity.this));
        rvCustomerlist.setAdapter(adapter);

    }


    private void show_bottom_sheet(View view, String cust_name) {
        int itemPosition = rvCustomerlist.getChildLayoutPosition(view);
        //OGtoast.OGtoast(""+itemPosition , CustomerlistActivity.this);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        btm_text_cust_name.setText(cust_name);

        CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) _fab.getLayoutParams();
        p.setAnchorId(R.id.bottom_sheet);
        _fab.setLayoutParams(p);
    }

    private void create_snack_bar(View view) {
        snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE);


        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();

        LayoutInflater mInflater = LayoutInflater.from(CustomerlistActivity.this);
        View snackView = mInflater.inflate(R.layout.snackbar_customer_list, null);


        TextView txt_geo_location = (TextView) snackView.findViewById(R.id.txt_geo_location);
        TextView txt_logout = (TextView) snackView.findViewById(R.id.txt_logout);

        txt_logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });


        snackbar.setCallback(new Snackbar.Callback() {

            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                _fab.setVisibility(View.VISIBLE);
            }

            @Override
            public void onShown(Snackbar snackbar) {

            }
        });
        layout.addView(snackView);
        layout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction() == MotionEvent.ACTION_UP) {

                    if (snackbar != null) {
                        snackbar.dismiss();
                    }

                    return true;
                } else {
                    return false;
                }

            }
        });
        snackbar.show();
    }

    private void Show_pending_bills() {
    //    Log.d("Pending", "ALL ORDERS ARE SHOWING");
        Intent intent = new Intent(CustomerlistActivity.this, IndividualPendingBillsActivity.class);
        intent.putExtra(CHEMIST_ID, Selected_chemist_id);
        intent.putExtra(CLIENT_NAME, Selected_client_name);
        startActivity(intent);
    }

    private void Show_Orders() {
    //    Log.d("SHOWORDERS", "ALL ORDERS ARE SHOWING");
        Intent intent = new Intent(CustomerlistActivity.this, Order_list.class);
        intent.putExtra(SELECTED_CHEMIST_ID, Selected_chemist_id);
        startActivity(intent);
    }

    private void Show_sales_returns() {
        Intent intent = new Intent(CustomerlistActivity.this, SalesReturnActivity.class);
        startActivity(intent);
    }

    private void Create_new_order() {
        Intent intent = new Intent(CustomerlistActivity.this, Create_Order_Salesman.class);
        intent.putExtra(CHEMIST_STOCKIST_NAME, Selected_client_name);
        intent.putExtra("client_id", Selected_chemist_id);
        startActivity(intent);
        globalVariable.setFromCustomerList(true);
    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {
    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {

        if (response != null) {
            try {
                if (f_name.equals(AppConfig.GET_STOCKIST_CUSTOMERLIST_STOCKISTLOGIN)) {
                    //Log.e("Customers",response.toString());

                    mStokiestmodels = new ArrayList<m_stokiest_distributor_list>();
                    String jsonData = response.toString();

                    //       Log.d("print_Distributors", jsonData);
                    if (!jsonData.isEmpty()) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        mStokiestmodels = Arrays.asList(gson.fromJson(jsonData, m_stokiest_distributor_list[].class));
                    }
                    stokiest_recycleview(mStokiestmodels);
                }

                if (f_name.equals(AppConfig.GET_STOCKIST_CUSTOMERLIST)) {
                    mModels = new ArrayList<m_customerlist>();
                    String jsondata = response.toString();
                    if (!jsondata.isEmpty()) {
                        Log.d("jsondata",jsondata.toString());
                        GsonBuilder builder = new GsonBuilder();
                        Gson mGson = builder.create();
                        mModels = Arrays.asList(mGson.fromJson(jsondata, m_customerlist[].class));
                    }
                    update_recycleview(mModels);
                }

            } catch (Exception e) {
                e.toString();
            }
        }
    }

    private void stokiest_recycleview(final List<m_stokiest_distributor_list> mStokiestmodels) {

        //Past code Here
        txt_cust_count.setText(mStokiestmodels.size() + " Customers");
        adapter = new RecyclerView.Adapter<BindingViewHolder>() {
            @Override
            public BindingViewHolder onCreateViewHolder(ViewGroup parent,final int viewType) {
                LayoutInflater inflater = LayoutInflater.from(CustomerlistActivity.this);
                ViewDataBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragement_stokiest_customerlist_items, parent, false);
                //fragement_stokiest_customerlist_items
                return new BindingViewHolder(binding.getRoot());
            }


            @Override
            public void onBindViewHolder(BindingViewHolder holder, final int position) {
                m_stokiest_distributor_list stock_customerlist = mStokiestmodels.get(position);
                holder.getBinding().setVariable(BR.v_stokiestcustomerlist, stock_customerlist);
                holder.getBinding().executePendingBindings();
                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        show_bottom_sheet(v, mStokiestmodels.get(position).getClient_LegalName());
                        Selected_chemist_id = mStokiestmodels.get(position).getClient_Code();
                        selected_Ten_orders = Selected_chemist_id;
                        Selected_client_name = mStokiestmodels.get(position).getClient_LegalName();
                        txt_name.setText(mStokiestmodels.get(position).getClient_LegalName());
                        txt_cus_code.setText(mStokiestmodels.get(position).getClient_Code());
                        txt_cus_address.setText(mStokiestmodels.get(position).getCityName());
                        txt_cus_email.setText(mStokiestmodels.get(position).getClient_Email());
                        txt_cus_phone.setText(mStokiestmodels.get(position).getClient_Contact());
                        /*txt_cus_phone.setText(post.get(position).getEmail_id());
                        if (post.get(position).getOutstanding_Bill() != null) {
                            txt_cus_outstanding.setText(post.get(position).getOutstanding_Bill());
                        }*/
                    }
                });


                /*m_customerlist omline_customerlist = post.get(position);
                holder.getBinding().setVariable(BR.v_customerlist, omline_customerlist);
                holder.getBinding().executePendingBindings();

                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {


                        show_bottom_sheet(v, post.get(position).getCustomerName());

                        Selected_chemist_id = post.get(position).getChemist_id();

                        selected_Ten_orders = Selected_chemist_id;

                        Selected_client_name = post.get(position).getCustomerName();
                        txt_name.setText(post.get(position).getCustomerName());
                        txt_cus_code.setText(post.get(position).getCust_Code());
                        txt_cus_address.setText(post.get(position).getLocation());
                        txt_cus_email.setText(post.get(position).getEmail_id());
                        txt_cus_phone.setText(post.get(position).getEmail_id());
                        if (post.get(position).getOutstanding_Bill() != null) {
                            txt_cus_outstanding.setText(post.get(position).getOutstanding_Bill());
                        }

                    }
                });*/


            }

            @Override
            public int getItemCount() {
                return mStokiestmodels.size();
            }
        };
        rvCustomerlist.setLayoutManager(new LinearLayoutManager(CustomerlistActivity.this));
        rvCustomerlist.setAdapter(adapter);
    }

    void filter_on_text(String newText) {
        newText.toLowerCase();
        List<m_customerlist> filteredModelList = new ArrayList<>();
        for (m_customerlist model : mModels) {
            final String text = model.getCustomerName().toLowerCase();
            if (text.contains(newText)) {
                filteredModelList.add(model);
            }
        }
        update_recycleview(filteredModelList);
        rvCustomerlist.scrollToPosition(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(CustomerlistActivity.this,MainActivity.class);
        startActivity(i);
    }
}
