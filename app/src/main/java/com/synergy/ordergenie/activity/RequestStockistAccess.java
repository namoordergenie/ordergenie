package com.synergy.ordergenie.activity;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.OGtoast;

import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class RequestStockistAccess extends AppCompatActivity  implements MakeWebRequest.OnResponseSuccess{


    @BindView(R.id.txt_user_selection)
    TextView _txt_user_selection;
    @BindView(R.id.txt_distrubutor_code)
    TextView txt_distrubutor_code;
    @BindView(R.id.edt_message)
    EditText edt_message;



    @OnClick(R.id.btn_cancel)
    public void btncancel() {
         finish();
    }

    SharedPreferences pref;
    private String stockist_id,stockist_name;
     static final String  CHEMIST_STOCKIST_ID = "chemist_stockist_id";
    public static final String  CHEMIST_STOCKIST_NAME = "chemist_stockist_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_stockist_access);
            ButterKnife.bind(this);
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        get_intent();



    }


    @OnClick(R.id.save_btn)
    public void send_request() {
        post_access_request(edt_message.getText().toString());
    }

    void get_intent()
    {
        stockist_id=getIntent().getStringExtra(CHEMIST_STOCKIST_ID);
        stockist_name=getIntent().getStringExtra(CHEMIST_STOCKIST_NAME);

        txt_distrubutor_code.setText("Distributor Code : "+stockist_id);
        _txt_user_selection.setText(stockist_name);
    }

    void post_access_request(String messsage)
    {
        String client_id=pref.getString(CLIENT_ID,"");

        JSONObject jsonParams = new JSONObject();

        try {
            jsonParams.put("StockistID", stockist_id);
            jsonParams.put("ClientID", pref.getString(CLIENT_ID, ""));
            jsonParams.put("Type", "accessrequest");
            jsonParams.put("Info", messsage);
            jsonParams.put("active", "true");

            MakeWebRequest.MakeWebRequest("Post", AppConfig.POST_CHEMIST_TO_STOCKIST_INVENTORY_ACESS, AppConfig.POST_CHEMIST_TO_STOCKIST_INVENTORY_ACESS, jsonParams, this, true);
        }catch (Exception e)
        {

        }
    }


    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {

        if(response!=null)
        {
            if(f_name.equals(AppConfig.POST_CHEMIST_TO_STOCKIST_INVENTORY_ACESS))
            {
                OGtoast.OGtoast("Request sent succesfully",RequestStockistAccess.this);
                finish();
            }
        }
    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {

    }
}
