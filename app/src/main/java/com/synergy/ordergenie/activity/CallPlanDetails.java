package com.synergy.ordergenie.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.databinding.library.baseAdapters.BR;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;
import com.synergy.ordergenie.model.m_call_activities;
import com.synergy.ordergenie.model.m_customerlist;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.SaveCallPlan;
import com.synergy.ordergenie.utils.get_current_location;

import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_NAME;
import static com.synergy.ordergenie.utils.ConstData.user_info.IS_ORDER_DELIVERY_ASSIGNED;
import static com.synergy.ordergenie.utils.ConstData.user_info.IS_PAYMENT_COLLECTION_ASSIGNED;
import static com.synergy.ordergenie.utils.ConstData.user_info.IS_TAKE_ORDER_ASSIGNED;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class CallPlanDetails extends AppCompatActivity implements MakeWebRequest.OnResponseSuccess {


    AppController globalVariable;
    private static final int NOTIFICATION_ID = 147894;
    private String s_response_array;
    String chemist_id="";
    public static final String CHEMIST_STOCKIST_NAME = "chemist_stockist_name";
    SharedPreferences pref;
    public static final String SELECTED_CHEMIST_ID = "selected_chemist_id";
    String Start_time, End_time;
    private JSONObject taskStatusData = new JSONObject();
    private Boolean isAnyTaskCompleted = false;
    private String call_plan_date;
    SharedPreferences.Editor editor;

    private int TaskPendingCount = 0;
    MyResultReceiver resultReceiver;

    String Name = "";


    @BindView(R.id.chrono)
    Chronometer stopWatch;

    @BindView(R.id.Client_name)
    TextView Client_name;

    @BindView(R.id.txt_time)
    TextView txt_time;
    @BindView(R.id.ts_txt_time)
    TextView ts_txt_time;

    @BindView(R.id.del_txt_description)
    TextView del_txt_description;


    @BindView(R.id.txt_description)
    TextView txt_description;

    @BindView(R.id.cp_txt_description)
    TextView cp_txt_description;

    @BindView(R.id.cp_txt_time)
    TextView cp_txt_time;

    @BindView(R.id.txt_date_select)
    TextView txt_date_select;

    @BindView(R.id.pending_count)
    TextView pending_count;


    @BindView(R.id.Cust_adress)
    TextView Cust_adress;

    @BindView(R.id.toolbar)
    Toolbar _toolbar;

    @BindView(R.id.take_order)
    RelativeLayout take_order;

    @BindView(R.id.collect_payment)
    RelativeLayout collect_payment;

    @BindView(R.id.Order_delivery)
    RelativeLayout Order_delivery;


    @OnClick(R.id.Order_delivery)
    void order_delivery() {
        if (del_txt_description.getText().toString().equals("Completed")) {
            // OGtoast.OGtoast("This task is completed", CallPlanDetails.this);
            create_order_delivery();
        } else {

            create_order_delivery();
        }

    }

    @OnClick(R.id.take_order)
    void order() {

        if (txt_description.getText().toString().equals("Completed")) {
            //Log.d("clickTakeOrder11", "yes clk Take Order");
            create_order();
            //OGtoast.OGtoast("This task is completed", CallPlanDetails.this);
        } else {

         //   Log.d("clickTakeOrder12", "yes Take Order");
            create_order();
        }

    }

    @OnClick(R.id.collect_payment)
    void payment() {

        if (cp_txt_description.getText().toString().equals("Completed")) {
            collect_payment();
            // OGtoast.OGtoast("This task is completed", CallPlanDetails.this);
        } else {

            collect_payment();
        }

    }


    @OnClick(R.id.Order_delivery)
    void Order_delivery() {

    }

   /*@OnClick(R.id.take_selfie)
    void take_selfie()
    {
        Take_selfie();
    }*/

    @OnClick(R.id.stop_call)
    void end_Call() {
        end_call();
    }

    @OnClick(R.id.end_call)
    void endCall() {
        end_call();
    }

    @OnClick(R.id.btn_show_route)
    void show_route() {
        Intent i = new Intent(CallPlanDetails.this, MapsActivity.class);
        i.putExtra("response", s_response_array);
        startActivity(i);
    }


    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    TextView profile;
    ArrayList<m_customerlist> mModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_plan_details);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;

        }
        ButterKnife.bind(this);
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        globalVariable = (AppController) getApplicationContext();
        setSupportActionBar(_toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this ,R.color.colorPrimaryDark));
        }
        new get_current_location(CallPlanDetails.this);
        profile = (TextView) findViewById(R.id.view_profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(CallPlanDetails.this, ProfileActivity.class);
//                startActivity(intent);
                mModels = new ArrayList<m_customerlist>();
                for (int i = 0; i < mModels.size(); i++) {
                    Log.e("ChemistId", mModels.get(i).getChemist_id());
                }
            }
        });
        // new get_current_location(CallPlanDetails.this);
        editor = pref.edit();

        String task = getIntent().getStringExtra("task_status");
        call_plan_date = getIntent().getStringExtra("filter_start_date");

        try {
            if (task != null) {

                taskStatusData = new JSONObject(task);

            } else {
                taskStatusData.put("1", "no");
                taskStatusData.put("2", "no");
                taskStatusData.put("3", "no");
            }
        } catch (Exception e) {

        }

        resultReceiver = new MyResultReceiver(null);
        show_hide_tasks();
        SharedPreferences prefs = getSharedPreferences("MY PREF", MODE_PRIVATE);
        String client_name = prefs.getString("client_name", null);
        chemist_id=prefs.getString("chemist_id", null);


        Log.d("printpaymentId",chemist_id);

        if (client_name != null) {
            Name = prefs.getString(CLIENT_NAME, "");//"No name defined" is the default value.
            //0 is the default value.
        }
        // Client_name.setText(getIntent().getStringExtra("client_name"));
        Client_name.setText(Name);
        Cust_adress.setText(getIntent().getStringExtra("client_location"));

        create_notification();

        txt_date_select.setText(dateFormat.format(Calendar.getInstance().getTime()));
        s_response_array = getIntent().getStringExtra("response");
        stopWatch.setText("00:00:00");

        stopWatch.start();
        stopWatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String hh = h < 10 ? "0" + h : h + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                cArg.setText(hh + ":" + mm + ":" + ss);

            }
        });
    }

    private void fill_call_plan_activities(final List<m_call_activities> cal_activity_array) {
        final RecyclerView.Adapter<BindingViewHolder> adapter_activities = new RecyclerView.Adapter<BindingViewHolder>() {
            @Override
            public BindingViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
                ViewDataBinding binding = null;
                LayoutInflater inflater = LayoutInflater.from(CallPlanDetails.this);
                binding = DataBindingUtil
                        .inflate(inflater, R.layout.adapter_call_activities_list, parent, false);

                return new BindingViewHolder(binding.getRoot());
            }

            @Override
            public void onBindViewHolder(BindingViewHolder holder, int position) {
                m_call_activities call_activity_list = cal_activity_array.get(position);
                holder.getBinding().setVariable(BR.v_call_plan_activities, call_activity_list);
                holder.getBinding().executePendingBindings();
            }

            @Override
            public int getItemCount() {
                return cal_activity_array.size();
            }

            @Override
            public int getItemViewType(int position) {

                return position;
            }
        };

       /* rv_call_activities.setLayoutManager(new LinearLayoutManager(this));
        rv_call_activities.setHasFixedSize(false);
        rv_call_activities.setLayoutManager(new LinearLayoutManager(CallPlan.this));
        rv_call_activities.setAdapter(adapter_activities);
*/
    }


    private class BindingViewHolder extends RecyclerView.ViewHolder {

        public BindingViewHolder(View itemView) {
            super(itemView);
        }

        public ViewDataBinding getBinding() {
            return DataBindingUtil.getBinding(itemView);
        }
    }

    private void create_order()
    {
        Intent i = new Intent(this, Create_Order_Salesman.class);
        i.putExtra("Call_plan_id", chemist_id);
        i.putExtra("receiver", resultReceiver);
        i.putExtra("client_id", getIntent().getStringExtra("chemist_id"));
        i.putExtra(CHEMIST_STOCKIST_NAME, Name);
        txt_time.setText("Task Started at : " + dateFormat.format(Calendar.getInstance().getTime()));
        Start_time = dateFormat.format(Calendar.getInstance().getTime());
        txt_description.setText("Task in progress");
        globalVariable.setCall_plan_Started(true);
        startActivity(i);
    }

    private void create_order_delivery() {

        Start_time = dateFormat.format(Calendar.getInstance().getTime());
        Intent i = new Intent(this, Order_list_delivery.class);
        i.putExtra("type","Delivery");
        i.putExtra("Call_plan_id", getIntent().getStringExtra("Call_plan_id"));
        i.putExtra("receiver", resultReceiver);
        i.putExtra("s_response_array", s_response_array);
        i.putExtra("Start_time", Start_time);
        i.putExtra(CLIENT_NAME, Name);
        i.putExtra("call_plan_task", true);
        i.putExtra(SELECTED_CHEMIST_ID, getIntent().getStringExtra("chemist_id"));
        i.putExtra(CHEMIST_STOCKIST_NAME, getIntent().getStringExtra("client_name"));
        ts_txt_time.setText("Task Started at : " + dateFormat.format(Calendar.getInstance().getTime()));
        del_txt_description.setText("Task in progress");
        globalVariable.setCall_plan_Started(true);
        startActivity(i);

    }

    private void collect_payment() {
        cp_txt_time.setText("Task Started at : " + dateFormat.format(Calendar.getInstance().getTime()));
        Intent i = new Intent(this, IndividualPendingBillsActivity.class);
       // Intent i = new Intent(this, SalesmanMakePayment.class);
        i.putExtra("receiver", resultReceiver);
        i.putExtra("call_plan_task", true);
        i.putExtra(CLIENT_NAME, Name);
        i.putExtra("Call_plan_id", getIntent().getStringExtra("Call_plan_id"));
        i.putExtra("chemist_id", getIntent().getStringExtra("chemist_id"));
        startActivity(i);
    }

    private void Order_delivery_start() {
        Intent i = new Intent(this, Payment_Option.class);
        startActivity(i);

    }

    private void Take_selfie() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);

    }

    private void end_call() {
        globalVariable.setCall_plan_Started(false);
        NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nMgr.cancel(NOTIFICATION_ID);


        if (isAnyTaskCompleted) {
            try {
                JSONArray j_arr = new JSONArray(s_response_array);
                Log.e("j_arr", j_arr.toString());

                new SaveCallPlan(CallPlanDetails.this, call_plan_date, j_arr.getJSONObject(0).getString("StockistCallPlanID"),
                        getIntent().getStringExtra("chemist_id"), pref.getString(USER_ID, "0"), Start_time, End_time, "1", taskStatusData.toString());

            } catch (Exception e) {
                e.toString();
            }
        }

        finish();
    }

    void create_notification() {
        // Intent intent = new Intent(this,CallPlanDetails.class);
//        Intent intent = new Intent();
//
//       // intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//
//       // intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent1 = new Intent(this, CallPlanDetails.class);
        intent1.setAction(Intent.ACTION_MAIN);
        intent1.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent1, 0);




        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                // Set Icon
                .setSmallIcon(R.drawable.logo)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentTitle("OG Call")
                .setContentText("Call started for : " + Name)
                // Set Ticker Message
                .setTicker("OrderGenie")
                .setUsesChronometer(true)
                .setProgress(100, 100, true)
                // Dismiss Notification
                .setAutoCancel(false)
                // Set PendingIntent into Notification
               // .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this.getIntent()), PendingIntent.FLAG_UPDATE_CURRENT));

        .setContentIntent(pendingIntent);



               //****** .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, CallPlanDetails.class), PendingIntent.FLAG_UPDATE_CURRENT));
        //.setContentIntent(contentIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        builder.setOngoing(true);
        mNotificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {

            back_callPlan_currentCall();
            //Log.d("todaybackwindow","Show Here dialog");
          //  finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void back_callPlan_currentCall() {

        new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Do you want to End Current call ?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        end_Call();
                        finish();
                        //  System.exit(0);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();

    }

    @Override
    public void onBackPressed() {

        back_callPlan_currentCall();
        //end_Call();
//        super.onBackPressed();
        // finish();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    class MyResultReceiver extends ResultReceiver {
        public MyResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {


            if (resultCode == 200) {
                isAnyTaskCompleted = true;
                TaskPendingCount = TaskPendingCount - 1;
                pending_count.setText("" + TaskPendingCount);
                txt_description.setText("Completed");

                txt_time.setText("Call completed at : " + dateFormat.format(Calendar.getInstance().getTime()));
                End_time = dateFormat.format(Calendar.getInstance().getTime());
                try {
                    //  JSONArray j_arr = new JSONArray(s_response_array);

                    taskStatusData.put("1", "yes");
//                    editor.putBoolean(IS_TAKE_ORDER_ASSIGNED, true);
//                    editor.commit();
//                    Log.e("IS_TAKE_ORDER_ASSIGNED",pref.getBoolean(IS_TAKE_ORDER_ASSIGNED,false)+"");
                    // new SaveCallPlan(CallPlanDetails.this, j_arr.getJSONObject(0).getString("StockistCallPlanID"),
                    //    getIntent().getStringExtra("client_id"), pref.getString(USER_ID, "0"), Start_time, End_time, "2", "1", "yes");
                } catch (Exception e) {
                    e.toString();
                }
            } else if (resultCode == 400) {
                isAnyTaskCompleted = true;
                TaskPendingCount = TaskPendingCount - 1;

                pending_count.setText("" + TaskPendingCount);
                del_txt_description.setText("Completed");

                ts_txt_time.setText("Call completed at : " + dateFormat.format(Calendar.getInstance().getTime()));
                End_time = dateFormat.format(Calendar.getInstance().getTime());
                try {
                    // JSONArray j_arr = new JSONArray(s_response_array);

                    taskStatusData.put("3", "yes");
//                    editor.putBoolean(IS_ORDER_DELIVERY_ASSIGNED, true);
//                    editor.commit();
//
//                    Log.e("IS_ORDER_DELIVER",pref.getBoolean(IS_ORDER_DELIVERY_ASSIGNED,false)+"");


                    //   new SaveCallPlan(CallPlanDetails.this, j_arr.getJSONObject(0).getString("StockistCallPlanID"),
                    // getIntent().getStringExtra("chemist_id"), pref.getString(USER_ID, "0"), Start_time, End_time, "2", "3", "yes");

                } catch (Exception e) {
                    e.toString();
                }
            } else if (resultCode == 300) {

                isAnyTaskCompleted = true;
                TaskPendingCount = TaskPendingCount - 1;

                pending_count.setText("" + TaskPendingCount);
                cp_txt_description.setText("Completed");

                cp_txt_time.setText("Call completed at : " + dateFormat.format(Calendar.getInstance().getTime()));
                End_time = dateFormat.format(Calendar.getInstance().getTime());
                try {
                    // JSONArray j_arr = new JSONArray(s_response_array);

                    taskStatusData.put("2", "yes");

//                    editor.putBoolean(IS_PAYMENT_COLLECTION_ASSIGNED, true);
//                    editor.commit();
//
//                    Log.e("IS_PAYMENT_COLLECTION",pref.getBoolean(IS_PAYMENT_COLLECTION_ASSIGNED,false)+"");
                    //  new SaveCallPlan(CallPlanDetails.this, j_arr.getJSONObject(0).getString("StockistCallPlanID"),
                    //    getIntent().getStringExtra("chemist_id"), pref.getString(USER_ID, "0"), Start_time, End_time, "2", "2", "yes");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void show_hide_tasks() {
        if (pref.getBoolean(IS_TAKE_ORDER_ASSIGNED, false)) {
            take_order.setVisibility(View.VISIBLE);
            TaskPendingCount = TaskPendingCount + 1;
        } else {
            take_order.setVisibility(View.GONE);
        }
        if (pref.getBoolean(IS_PAYMENT_COLLECTION_ASSIGNED, false)) {
            collect_payment.setVisibility(View.VISIBLE);
            TaskPendingCount = TaskPendingCount + 1;
        } else {
            collect_payment.setVisibility(View.GONE);
        }
        if (pref.getBoolean(IS_ORDER_DELIVERY_ASSIGNED, false)) {
            TaskPendingCount = TaskPendingCount + 1;
            Order_delivery.setVisibility(View.VISIBLE);
        } else {
            Order_delivery.setVisibility(View.GONE);
        }


        try {
            if (taskStatusData.getString("1").equals("yes")) {
                txt_description.setText("Completed");
                TaskPendingCount = TaskPendingCount - 1;
            }

            if (taskStatusData.getString("2").equals("yes")) {
                cp_txt_description.setText("Completed");
                TaskPendingCount = TaskPendingCount - 1;
            }

            if (taskStatusData.getString("3").equals("yes")) {
                del_txt_description.setText("Completed");
                TaskPendingCount = TaskPendingCount - 1;
            }

            if (taskStatusData.getString("1").equals("yes") && taskStatusData.getString("2").equals("yes") && taskStatusData.getString("3").equals("yes")) {

                TaskPendingCount = 3;
             /*   editor.remove(IS_TAKE_ORDER_ASSIGNED);
                editor.remove(IS_PAYMENT_COLLECTION_ASSIGNED);
                editor.remove(IS_ORDER_DELIVERY_ASSIGNED);*/
                editor.apply();
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
        pending_count.setText("" + TaskPendingCount);
    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {

        if (response != null) {
            try {
                Log.e("Saveplan", response.toString());
                if (f_name.equals(AppConfig.POST_SAVE_CALL_PLAN)) {

                }
            } catch (Exception e) {
                e.toString();
            }
        }
    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {

        if (response != null) {
            try {


                if (f_name.equals(AppConfig.GET_STOCKIST_CALL_PLAN)) {

                }


            } catch (Exception e) {
                e.toString();
            }
        }


    }

}
