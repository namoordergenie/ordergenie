package com.synergy.ordergenie.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.synergy.ordergenie.BR;
import com.synergy.ordergenie.Helper.SQLiteHandler;
import com.synergy.ordergenie.R;
import com.synergy.ordergenie.adapter.BindingViewHolder;
import com.synergy.ordergenie.adapter.ad_AutocompleteCustomArray;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;
import com.synergy.ordergenie.database.ChemistCart;
import com.synergy.ordergenie.database.ChemistCartDao;
import com.synergy.ordergenie.database.DaoSession;
import com.synergy.ordergenie.database.MasterPlacedOrder;
import com.synergy.ordergenie.database.StockistProducts;
import com.synergy.ordergenie.utils.BadgeDrawable;
import com.synergy.ordergenie.utils.ConstData;
import com.synergy.ordergenie.utils.CustomAutoCompleteView;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.OGtoast;
import com.synergy.ordergenie.utils.RefreshData;

import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class Create_Order_Salesman extends AppCompatActivity implements MakeWebRequest.OnResponseSuccess {

    ArrayList<StockistProducts> posts = new ArrayList<StockistProducts>();
    List<StockistProducts> sreach_product_list = new ArrayList<StockistProducts>();
    private SharedPreferences pref;
    private SQLiteHandler db;
    ProgressDialog progressDialog;
    String Stockist_Id;
    private EditText editText,txt_comment;
    Boolean Clicked_cart = false;
    public static final String CHEMIST_STOCKIST_ID = "chemist_stockist_id";
    public static final String CHEMIST_STOCKIST_NAME = "chemist_stockist_name";
    // List<ChemistCart> cart;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
   // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private Snackbar snackbar;
    private BottomSheetBehavior behavior;
    private String Doc_Id, client_id;
    private String legend_data,legend_mode,legendName;
    private String call_plan_customer_id;
    private Boolean Cart_Id_available = false;
    private Menu mToolbarMenu;
    private Integer n_product_cart_count=0;
    AppController globalVariable;
    private String Client_id;
    ResultReceiver resultReceiver;
    private  TextView txt_cust_name,txt_order_id,txt_total_items,txt_total;
    // @BindView(R.id.autoCompleteTextView)
    CustomAutoCompleteView _autoCompleteTextView;
    ad_AutocompleteCustomArray adpter;
    private ChemistCartDao chemistCartDao;
    TextView mfg, Dosefrom, Packsize, PTR, MRP, Scheme;
    Toolbar toolbar;
    FragmentManager fm = getSupportFragmentManager();

    @BindView(R.id.stock)
    TextView stock;

    @BindView(R.id.btnminus)
    Button _btnminus;

    @BindView(R.id.rv_Cartdatalist)
    RecyclerView _rv_Cartdatalist;

    @BindView(R.id.btnplus)
    Button _btnplus;

    @BindView(R.id.Qty)
    EditText _Qty;

    @BindView(R.id.OrderAmt)
    TextView _OrderAmt;

    @BindView(R.id.remark)
    TextView _remark;

    @BindView(R.id.txt_customer_name)
    TextView _txt_customer_name;

    @BindView(R.id.addProduct)
    Button _addProduct;

    @BindView(R.id.cancelOrder)
    Button _cancelOrder;

    @BindView(R.id.confirmOrder)
    Button _confirmOrder;

    @BindView(R.id.bottom_sheet)
    RelativeLayout _bottumLayout;

    @Nullable
    @BindView(R.id.fab)
    FloatingActionButton _fab;

    @BindView(R.id.main_coordinate)
    CoordinatorLayout _main_coordinate;

    @BindView(R.id.progress)
    ProgressBar progress;

    @OnClick(R.id.fab)
    void onclickfab(View view) {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            //  get_CartData();
            // _fab.setVisibility(View.GONE);
        }
    }

    int Qty = 1;

    Integer type;
    String Itemname, Pack, MfgName, DoseForm, Sche, Remark, Itemcode, MfgCode, ProductId, UOM;
    Float vMrp, vRate;
    DaoSession daoSession;

    String login_type;
    private String User_id;
    private List<ChemistCart> chemistCartList;
    float orderAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__order);

        //Log.d("salesman","using salesman");
        ButterKnife.bind(this);
        db = new SQLiteHandler(this);


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(_rv_Cartdatalist.getContext(),
                DividerItemDecoration.VERTICAL);
        _rv_Cartdatalist.addItemDecoration(dividerItemDecoration);


        // setSupportActionBar(toolbar);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);

        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        resultReceiver = getIntent().getParcelableExtra("receiver");
        SharedPreferences prefs = getSharedPreferences("MY PREF", MODE_PRIVATE);
      //
        // ***call_plan_customer_id = prefs.getString("chemist_id", null);
       call_plan_customer_id = getIntent().getStringExtra("client_id");
        globalVariable = (AppController) getApplicationContext();
        login_type = pref.getString(ConstData.user_info.CLIENT_ROLE, "");

        User_id = pref.getString(USER_ID, "0");
        Client_id = pref.getString(CLIENT_ID, "0");
        editText=(EditText)findViewById(R.id.edit_comment);
        _txt_customer_name.setText(getIntent().getStringExtra(CHEMIST_STOCKIST_NAME));
        _autoCompleteTextView = (CustomAutoCompleteView) findViewById(R.id.autoCompleteTextView);
        mfg = (TextView) findViewById(R.id.mfg);
        //final TextView mfg=(TextView)mContext.getApplicationInfo().findViewById(R.id.mfg);

        Dosefrom = (TextView) findViewById(R.id.Doseform);
        Packsize = (TextView) findViewById(R.id.Packsize);

        PTR = (TextView) findViewById(R.id.PTR);
        MRP = (TextView) findViewById(R.id.MRP);
        Scheme = (TextView) findViewById(R.id.Scheme);

        behavior = BottomSheetBehavior.from(_bottumLayout);
        client_id = pref.getString(CLIENT_ID, "");
        daoSession = ((AppController) getApplication()).getDaoSession();
        chemistCartDao = daoSession.getChemistCartDao();
        //setSupportActionBar(mToolbarMenu);
        init();

        get_stockist_legends();

       /* _autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(posts.size()>0) {
                    filter(_autoCompleteTextView.getText().toString());
                   *//* adpter = new ad_AutocompleteCustomArray(Create_Order_Salesman.this, sreach_product_list);
                    _autoCompleteTextView.setAdapter(adpter);*//*
                }else
                {
                    get_ProductList_json();
                }

            }
        });
*/
        _autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                try {
                    _addProduct.setEnabled(true);
                    _addProduct.setClickable(true);
                    _addProduct.setBackgroundColor(Color.parseColor("#03a9f4"));
                    _Qty.requestFocus();
                    _Qty.setText("");
                    //Student selected = (Student) arg0.getAdapter().getItem(arg2);

                    //arg1.getTag(R.id.key_product_Mfg);
                    //String fmgname=(String)_autoCompleteTextView.getTag(R.id.key_product_Mfg);
                    ProductId = arg1.getTag(R.id.key_product_code).toString();
                    Itemcode = (arg1.getTag(R.id.key_product_ItemCode).toString());
                    mfg.setText(String.valueOf(arg1.getTag(R.id.key_product_Mfg)));

                    _autoCompleteTextView.setText(String.valueOf(arg1.getTag(R.id.key_product_Name)));
                    if (String.valueOf(arg1.getTag(R.id.key_product_Dose)) == null || String.valueOf(arg1.getTag(R.id.key_product_Pack)).equals("null")) {
                        Dosefrom.setText("---");
                    } else {
                        Dosefrom.setText(String.valueOf(arg1.getTag(R.id.key_product_Dose)));
                    }

                    if (String.valueOf(arg1.getTag(R.id.key_product_Pack)) == null || String.valueOf(arg1.getTag(R.id.key_product_Pack)).equals("null")) {
                        Packsize.setText("---");
                    } else {
                        Packsize.setText(String.valueOf(arg1.getTag(R.id.key_product_Pack)));
                    }

                    if (String.valueOf(arg1.getTag(R.id.key_product_Scheme)) == null || String.valueOf(arg1.getTag(R.id.key_product_Scheme)).equals("null")||String.valueOf(arg1.getTag(R.id.key_product_Scheme)).equals("")) {
                        Scheme.setText("---");
                    } else {
                        Scheme.setText(String.valueOf(arg1.getTag(R.id.key_product_Scheme)));
                    }

                    if (!arg1.getTag(R.id.key_product_PTR).equals("null")) {
                        vRate = Float.parseFloat(arg1.getTag(R.id.key_product_PTR).toString());
                    } else {
                        vRate = (float) 0;
                    }
                    if (!arg1.getTag(R.id.key_product_MRP).equals("null")) {
                        vMrp = Float.parseFloat(arg1.getTag(R.id.key_product_MRP).toString());
                    } else {
                        vMrp = (float) 0;
                    }
                    if (arg1.getTag(R.id.key_product_Dose).equals("null")) {
                        UOM = arg1.getTag(R.id.key_product_Dose).toString();
                    }

                    if (!arg1.getTag(R.id.key_product_stock).equals("null")) {
                      //  setStockLegend(Integer.parseInt(arg1.getTag(R.id.key_product_stock).toString()));
                        set_stock_color_legend(Integer.parseInt(arg1.getTag(R.id.key_product_stock).toString()));
                    }


                    // arg1.getTag(R.id.key_product_stock).toString();
                    PTR.append(String.valueOf(vRate));
                    MRP.append(String.valueOf(vMrp));
                    //Scheme.setText(String.valueOf(arg1.getTag(R.id.key_product_Scheme)));
//-------------------------------------------------

                    MfgCode = (arg1.getTag(R.id.key_product_MfgCode).toString());
                    //    type = Integer.parseInt(arg1.getTag(R.id.key_product_Type).toString());
                    type = 0;

                    Itemname = (arg1.getTag(R.id.key_product_Name) != null ? arg1.getTag(R.id.key_product_Name).toString() : "NA");
                    // Pack = arg1.getTag(R.id.key_product_Pack).toString();
                    Pack = (arg1.getTag(R.id.key_product_Pack) == null) ? "NA" : arg1.getTag(R.id.key_product_Pack).toString();
                    MfgName = arg1.getTag(R.id.key_product_Mfg).toString();
                    DoseForm = arg1.getTag(R.id.key_product_Dose).toString();
                    Sche = arg1.getTag(R.id.key_product_Scheme).toString();
                    if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    {
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }

//                    Log.e("ProductId", ProductId);
//                    Log.e("DoseForm", DoseForm);
//                    Log.e("Sche", Sche);

                      //stock.setText(arg1.getTag(R.id.key_product_stock).toString());

                    //set_stock_color_legend(Integer.parseInt(arg1.getTag(R.id.key_product_stock).toString()));

                } catch (Exception e) {
                    e.printStackTrace();

                    Toast.makeText(getApplicationContext(), "Error occured", Toast.LENGTH_SHORT).show();
                }
                //comment by apurva to show keyboard

//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(_autoCompleteTextView.getWindowToken(), 0);

            }
        });



        //btnminus Button click Events

        _btnminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                // int num1;

                try {


                    String value = _Qty.getText().toString();
                    int num1 = Integer.parseInt(value);
                    if (num1 > 1) {
                        Qty = num1 - 1;
                    }

                    _Qty.setText(String.valueOf(Qty));
                }
                catch (Exception e)
                {

                }

            }
        });



        //btnplus Button click Events

        _btnplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    String value = _Qty.getText().toString();
                    if (!value.isEmpty()) {
                        int num1 = Integer.parseInt(value);
                        Qty = num1 + 1;
                    }
                    _Qty.setText(String.valueOf(Qty));
                }catch (Exception e)
                {

                }
            }
        });

        //addProduct Button click Events

        _addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    _addProduct.setEnabled(false);
                    _addProduct.setClickable(false);
                    _addProduct.setBackgroundColor(Color.parseColor("#d0ebfa"));

                    if (Itemcode != null && ProductId != null) {

                        //  Float price = vRate * Qty;
                        //addItemCart();
                        if (_autoCompleteTextView.getText().toString().isEmpty()) {
                            OGtoast.OGtoast("Please select a product", Create_Order_Salesman.this);
                        }
                        else {
//                            if (_Qty.getText().length() == 0) {
//                                OGtoast.OGtoast("Please select quantity", Create_Order_Salesman.this);
//
//                            } else {
                                if (_Qty.getText().toString().equals(""))
                                {
                                    Qty=1;

                                }
                                else
                                {
                                    Qty = Integer.parseInt(_Qty.getText().toString());
                                }
                                addItemCart();


                                // if (Cart_Id_available) {

                          /*  Log.e("status", "hiiiiiiiiii");
                            db.insert_into_chemist_cart_details(Doc_Id, Itemcode, ProductId, Qty, UOM, vRate.toString(), price.toString(),
                                    vMrp.toString(),
                                    dateFormat.format(Calendar.getInstance().getTime()));

                            price = price + db.get_total_order_amount(Doc_Id);
                            // Integer item_count =(db.get_total_order_item_count(Doc_Id))+1;
                            Integer item_count = (db.get_total_order_item_count(Doc_Id)) + 1;
                            Log.e("count", item_count.toString());
                            Log.e("count1", db.get_total_order_item_count(Doc_Id).toString());


                            _OrderAmt.setText(" Rs." + price.toString());
                            db.update_into_chemist_cart(Doc_Id, item_count, price.toString());


                            if (mToolbarMenu != null) {
                                createCartBadge(item_count);
                            }*/

                                // } else {
                           /* Log.e("status", "hiiiiiiiii222222222222i");
                            db.insert_into_chemist_cart(Doc_Id, call_plan_customer_id, price.toString(),
                                    dateFormat.format(Calendar.getInstance().getTime()),
                                    1, "cart", 0, 0);

                            db.insert_into_chemist_cart_details(Doc_Id, Itemcode, ProductId, Qty, UOM, vRate.toString(), price.toString(),
                                    vMrp.toString(),
                                    dateFormat.format(Calendar.getInstance().getTime()));
                            _OrderAmt.setText(" Rs." + price.toString());

                            if (mToolbarMenu != null) {
                                createCartBadge(1);
                            }
                            Cart_Id_available = true;*/
                                // }

                                //--Clearing TextView data after Adding to Cart---
                                mfg.setText("");
                                _autoCompleteTextView.setText("");
                            _autoCompleteTextView.requestFocus();
                                Dosefrom.setText("");
                                Packsize.setText("");
                                PTR.setText("");
                                MRP.setText("");
                                Scheme.setText("");
                                Qty = 1;
                                _Qty.setText("");
                                stock.setText("");
                               // stock.setBackgroundColor(Color.parseColor("#fff"));/*#fff*/
                                stock.setBackgroundColor(Color.WHITE);
                                Itemcode = null;
                                ProductId = null;
                                hideKeyboard();
                                // OGtoast.OGtoast("Product added to cart succesfully", Create_Order_Salesman.this);
                                //  Snackbar snackbar = Snackbar
                                //   .make(v, "Product added to cart succesfully", Snackbar.LENGTH_LONG);

                    /*    View view = snackbar.getView();
                        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                        params.gravity = Gravity.TOP;
                        view.setLayoutParams(params);

                        snackbar.show();  */
                          //  }
                        }
                    } else {
                        OGtoast.OGtoast("Please select a product", Create_Order_Salesman.this);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //cancelOrder Button click Events

        _cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_order(v);
            }
        });

        //confirmOrder Button click Events

        _confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   Cursor crs_cart = db.get_chemist_cart(call_plan_customer_id);

                // Log.d("cardid", String.valueOf(crs_cart));

                if (chemistCartList != null && chemistCartList.size() > 0)
                {
                    LayoutInflater inflater = LayoutInflater.from(Create_Order_Salesman.this);
                    final View dialogview = inflater.inflate(R.layout.fragment_order_summary, null);
                    final Dialog infoDialog = new Dialog(Create_Order_Salesman.this);//builder.create();
                    infoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    infoDialog.setContentView(dialogview);
                    txt_comment = (EditText) dialogview.findViewById(R.id.edit_comment);
                    txt_cust_name = (TextView) dialogview.findViewById(R.id.customer_name);
                    txt_order_id=(TextView)dialogview.findViewById(R.id.order_id);
                    txt_total_items=(TextView)dialogview.findViewById(R.id.total_items);
                     txt_total = (TextView) dialogview.findViewById(R.id.total_amount);
                    //  pgr.setVisibility(View.VISIBLE);
                    ChemistCart chemistCart = chemistCartList.get(0);
                    txt_cust_name.setText(getIntent().getStringExtra(CHEMIST_STOCKIST_NAME));
                    txt_order_id.setText(chemistCart.getDOC_ID());
                    txt_total_items.setText(String.valueOf(chemistCartList.size()));
                    txt_total.setText(String.valueOf(orderAmount));

                    dialogview.findViewById(R.id.confirm_order).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            confirm_dialog(txt_comment.getText().toString());
                            //confirm_order(txt_comment.getText().toString());
                            infoDialog.dismiss();
                        }
                    });
//                    dialogview.findViewById(R.id.cancel_order).setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view)
//                        {
//                            infoDialog.dismiss();
//                        }
//                    });


                    infoDialog.show();
                } else {
                    OGtoast.OGtoast("Cart is empty", Create_Order_Salesman.this);
                }

            }
        });


    }

//    _confirmOrder.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//
//            //   Cursor crs_cart = db.get_chemist_cart(call_plan_customer_id);
//
//            // Log.d("cardid", String.valueOf(crs_cart));
//
//            if (chemistCartList != null && chemistCartList.size() > 0) {
//
//
//
//                new AlertDialog.Builder(Create_Order_Salesman.this)
//                        .setTitle("Order")
//                        .setMessage("Do you wish to confirm your order?")
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which)
//                            {
//                                //confirm_order();
////                                    Fragment_OrderSummary_Dialog fragment = new Fragment_OrderSummary_Dialog();
////                                    fragment.show(fm, "Dialog Fragment");
//                                show_location_dialog();
//                                dialog.dismiss();
//
//                            }
//                        })
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int i) {
//                                dialog.dismiss();
//                            }
//                        })
//                        .show();
//
//            } else {
//                OGtoast.OGtoast("Cart is empty", Create_Order_Salesman.this);
//            }
//
//        }
//    });
//
//
//}

    void confirm_dialog(final String comment) {
                        new AlertDialog.Builder(Create_Order_Salesman.this)
                        .setTitle("Order")
                        .setMessage("Do you wish to confirm your order?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                confirm_order(comment);
                                dialog.dismiss();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        })
                        .show();


    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_autoCompleteTextView.getWindowToken(), 0);
    }
    private void init() {


        chemistCartList = chemistCartDao.getCartItem(call_plan_customer_id, true);
        if (chemistCartList != null) {
            n_product_cart_count = chemistCartList.size();
        }
       // get_stockist_inventory(Client_id);
        if (n_product_cart_count > 0) {
            for (int i = 0; i < chemistCartList.size(); i++) {
                orderAmount += Double.valueOf(chemistCartList.get(i).getAmount());
            }
        }

        _OrderAmt.setText("Rs." + String.format("%.2f", orderAmount));

       // get_ProductList_json();
    }


    private void setStockLegend(int stockNum) {
        if (stockNum > 300) {

            stock.setBackgroundColor(Color.parseColor("#5371D7"));

        } else if (stockNum > 200) {

            stock.setBackgroundColor(Color.parseColor("#5CDA6D"));

        } else if (stockNum > 100) {

            stock.setBackgroundColor(Color.parseColor("#EFE040"));

        } else {
            stock.setBackgroundColor(Color.parseColor("#EF9940"));

        }

    }


    private void addItemCart() {
      //  Log.e("Qty1",String.valueOf(Qty));
        Doc_Id = "OG" + sdf.format(Calendar.getInstance().getTime()) + (int) (Math.random() * 90) + 100;

        Float price = vRate * Qty;

      //  Log.d("PrintAmount", String.valueOf(price));

        ChemistCart chemistCart = new ChemistCart();

        chemistCart.setDOC_ID(Doc_Id);
        chemistCart.setItems(Itemcode);
        chemistCart.setItemname(Itemname);
        chemistCart.setProduct_ID(ProductId);
        chemistCart.setQty(String.valueOf(Qty));
        chemistCart.setUOM(UOM);
        chemistCart.setRate(vRate.toString());
        chemistCart.setPrice(price.toString());
        chemistCart.setMRP(String.valueOf(vMrp));
        chemistCart.setCreatedon(dateFormat.format(Calendar.getInstance().getTime()));
        chemistCart.setSalesman(true);
        chemistCart.setStockist_Client_id(call_plan_customer_id);
        chemistCart.setRemarks("cart");
        chemistCart.setOrder_sync(false);
        chemistCart.setSalesman(true);
        // chemistCart.setDoc_Date(dateFormat.format(Calendar.getInstance().getTime()));
        chemistCart.setAmount(price.toString());
        chemistCart.setPACK(Pack);
        //chemistCart.setStatus(false);
        //chemistCart.
        //chemistCart.se
//comment by apurva 20/9
//        price += orderAmount;
//        orderAmount = price;


        //  _OrderAmt.setText(" Rs." + String.format("%.2f", price));
        chemistCartDao.insertOrUpdateCart(chemistCart, true);
        chemistCartList.clear();
        chemistCartList = chemistCartDao.getCartItem(call_plan_customer_id, true);
        n_product_cart_count = chemistCartList.size();
        createCartBadge(n_product_cart_count);
        adpter.notifyDataSetChanged();


        orderAmount = 0;
        for (int i = 0; i < chemistCartList.size(); i++) {

            orderAmount += Double.valueOf(chemistCartList.get(i).getAmount());
        }
        _OrderAmt.setText(" Rs." + String.format("%.2f", orderAmount));
        // createCartBadge(chemistCartDao.ge);
        //Log.v("GreenDao", chemistCart.getId().toString());
        if (Qty==1)
        {
            OGtoast.OGtoast("one quantity added", Create_Order_Salesman.this);
        }
        else
        {
            OGtoast.OGtoast("Product added to cart succesfully", Create_Order_Salesman.this);
        }

    }

    public boolean onPrepareOptionsMenu(Menu paramMenu) {
        mToolbarMenu = paramMenu;

        if (n_product_cart_count != null) {
            createCartBadge(n_product_cart_count);
        }
        return super.onPrepareOptionsMenu(paramMenu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_order, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.action_cart) {
            hideKeyboard();
            Clicked_cart = true;
            if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            } else {

                if (chemistCartList.size() > 0) {
                    fill_Cartdata();
                } else {
                    OGtoast.OGtoast("Cart is empty", this);
                }
            }

        } else if (item.getItemId() == R.id.action_inventory) {
            show_inventory();
        }
        else if (item.getItemId() == android.R.id.home) {
          //  Log.d("Home_pressed","Home pressed");
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onResume() {
        super.onResume();
        //get_ProductList_json();
        //get_cart_id();

        Log.d("Activity", "onResume");
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        // get_ProductList_json();
    }



    private void get_ProductList_json() {

        //comment by apurva

        posts = new ArrayList<>();
        if (sreach_product_list.size()==0) {

            sreach_product_list = daoSession.getStockistProductsDao().loadAll();
        }
        //    Log.e("sreach_product_list", sreach_product_list.toString());
            posts = new ArrayList<>(sreach_product_list);
            adpter = new ad_AutocompleteCustomArray(this, posts,legend_data);

         //   _autoCompleteTextView.setThreshold(2);
            _autoCompleteTextView.setAdapter(adpter);
        //    Log.d("SizedArrList", String.valueOf(posts.size()));

    }
    public void filter(String charText) {
      /*  if(arraylist.size()<1) {
            this.arraylist.addAll(alternativeitems);
        }*/
        sreach_product_list.clear();
        if (charText.length() == 0) {
            // alternativeitems.addAll(arraylist);
        } else {
            for (StockistProducts wp : posts) {

                if (wp.getItemname().toUpperCase()
                        .contains(charText.toUpperCase())) {
                    sreach_product_list.add(wp);
                } else if (wp.getItemname()
                        .contains(charText)) {
                    sreach_product_list.add(wp);
                }
            }
        }

    }

    private void get_CartData() {


        Cursor crs_cart = db.get_saleman_cart_data(Doc_Id);
        String previousCartData = ConstData.get_jsonArray_from_cursor(crs_cart).toString();

    //    Log.e("previousCartData", previousCartData);// pref.getString("CartList", null);


        if (!previousCartData.isEmpty() || crs_cart.getCount() > 0) {
            GsonBuilder builder = new GsonBuilder();

            Gson mGson = builder.create();

            /*cart = new ArrayList<m_CartData>();
            cart = Arrays.asList(mGson.fromJson(previousCartData, m_CartData[].class));
*/
            if (chemistCartList.size() > 0) {
                fill_Cartdata();

            } else {

                OGtoast.OGtoast("Cart is empty", this);

            }


        } else {
            OGtoast.OGtoast("Cart is empty", this);
        }


    }

    private void fill_Cartdata() {
        final RecyclerView.Adapter<BindingViewHolder> adapter = new RecyclerView.Adapter<BindingViewHolder>() {
            @Override
            public BindingViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
                LayoutInflater inflater = LayoutInflater.from(Create_Order_Salesman.this);
                ViewDataBinding binding = DataBindingUtil
                        .inflate(inflater, R.layout.adpter_cartdata_list_new, parent, false);
//adpter_cartdata_list
                return new BindingViewHolder(binding.getRoot());
            }

            @Override
            public void onBindViewHolder(BindingViewHolder holder, final int position) {
                final ChemistCart chemistCart = chemistCartList.get(position);
                // Log.e("Cart_list", Cart_list.toString());
                holder.getBinding().setVariable(BR.v_cartDatalist, chemistCart);
                holder.getBinding().executePendingBindings();
                int POS = holder.getAdapterPosition();
                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                  //      Log.e("Click", "Item click");
                        _autoCompleteTextView.setText(chemistCartList.get(position).getItemname());
                        _autoCompleteTextView.requestFocus();
                        // Dosefrom.setText(chemistCartList.get(position).get());
//                        Packsize.setText(chemistCartList.get(position).getPACK());
//                        PTR.setText(chemistCartList.get(position).getRate());
//                        MRP.setText(chemistCartList.get(position).getMRP());
                        _Qty.setText(chemistCartList.get(position).getQty());
                        // _OrderAmt.setText(chemistCartList.get(position).getAmount());
                    //    Log.e("Item", chemistCartList.get(position).getItemname() + "\n" + chemistCartList.get(position).getPACK() + "\n" + chemistCartList.get(position).getMRP() + "\n" + chemistCartList.get(position).getRate() + "\n" + chemistCartList.get(position).getAmount());
                        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                    }
                });
                holder.getBinding().getRoot().findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //   Log.e("position",getAd);
                        // int pos = _rv_Cartdatalist.getAdapter().getItemId(position);
                        delete_product_from_cart(chemistCartList.get(position));
                        _autoCompleteTextView.setText("");
                        _Qty.setText("");
                        stock.setBackgroundColor(Color.WHITE);
                        _autoCompleteTextView.requestFocus();
                    }
                });

            }

            @Override
            public int getItemCount() {
                return chemistCartList.size();
            }

        };

        _rv_Cartdatalist.setLayoutManager(new LinearLayoutManager(this));
        _rv_Cartdatalist.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        _rv_Cartdatalist.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //At this point the layout is complete and the
                //dimensions of recyclerView and any child views are known.
                if (Clicked_cart) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    Clicked_cart = false;
                }
            }
        });
    }

    private void search_json(JSONArray j_array, String search_word) {
        for (int i = 0; i < j_array.length(); i++) {

        }
    }

    void get_cart_id() {

        if (call_plan_customer_id != null) {
            Cursor crs_cart = db.get_chemist_cart(call_plan_customer_id);

            if (crs_cart != null && crs_cart.getCount() > 0) {
                while (crs_cart.moveToNext()) {
                    Doc_Id = crs_cart.getString(crs_cart.getColumnIndex("DOC_NO"));
                    Cart_Id_available = true;
                    n_product_cart_count = (db.get_total_order_item_count(Doc_Id));
                 //   Log.e("Count11", n_product_cart_count.toString());
                    _OrderAmt.setText(" Rs." + crs_cart.getString(crs_cart.getColumnIndex("Amount")));

                    if (n_product_cart_count != 0) {
                        if (mToolbarMenu != null) {
                            createCartBadge(n_product_cart_count);
                        }

                    }

                }
            }


            if (Doc_Id == null) {
                Doc_Id = "OG" + sdf.format(Calendar.getInstance().getTime()) + (int) (Math.random() * 90) + 100;

                Cart_Id_available = false;

            }
        }
    }

    private void confirm_order(String Comment) {
        try {
//            Log.d("orderconfirm","yes Orderr confirm");
            //  if (Cart_Id_available) {
            Log.d("Comment",Comment);

            JSONArray main_j_array = new JSONArray();
            JSONObject main_j_object = new JSONObject();

            JSONArray line_j_array = new JSONArray();
            JSONArray productarray = new JSONArray();

            // main_j_object.put("Transaction_No","");
           // main_j_object.put("CreatedBy", Integer.parseInt(Client_id));
            main_j_object.put("CreatedBy",  pref.getString(USER_ID,""));

            // Cursor crs_cart = db.get_chemist_cart(call_plan_customer_id);

            chemistCartList = chemistCartDao.getCartItem(call_plan_customer_id, true);
            ChemistCart chemistCart = chemistCartList.get(0);

            if (chemistCartList.size() > 0) {

                main_j_object.put("Client_ID", Integer.parseInt(chemistCart.getStockist_Client_id()));
                main_j_object.put("DOC_NO", chemistCart.getDOC_ID());
                main_j_object.put("Doc_Date", chemistCart.getCreatedon());
                //main_j_object.put("Stockist_Client_id",Client_id+"  "+crs_cart.getString(crs_cart.getColumnIndex("Stockist_Client_id")));
                main_j_object.put("Stockist_Client_id", Integer.parseInt(Client_id));
            //    Log.d("stockisttID", client_id);
                main_j_object.put("Remarks", chemistCart.getRemarks());
                main_j_object.put("Items", chemistCartList.size());
                main_j_object.put("Amount", orderAmount);
                main_j_object.put("Status", 0);
                main_j_object.put("Createdon", chemistCart.getCreatedon());
               // main_j_object.put("Comments", editText.getText().toString());
               // main_j_object.put("LoginName",pref.getString(CLIENT_LOGIN_NAME,""));
                //main_j_object.put("USER_ID",pref.getString(USER_ID,""));

                //   Log.d("asdfdff", String.valueOf(Integer.parseInt(crs_cart.getString(crs_cart.getColumnIndex("status")))));
                Doc_Id = chemistCart.getDOC_ID();
                Cart_Id_available = true;

                //    }
                // Cursor crs_cart_details = db.get_chemist_cart_detail(Doc_Id);

                int i = 0;

                // if (crs_cart_details != null && crs_cart_details.getCount() > 0) {


                for (i = 0; i < chemistCartList.size(); i++) {

                    chemistCart = chemistCartList.get(i);
                    JSONObject line_j_object = new JSONObject();
                    line_j_object.put("Transaction_No", "");
                    //line_j_object.put("Doc_item_No", crs_cart_details.getString(crs_cart_details.getColumnIndex("Doc_item_No")));
                    line_j_object.put("Doc_item_No", i + 1);
                    line_j_object.put("Product_ID", Integer.parseInt(chemistCart.getProduct_ID()));
                    line_j_object.put("Qty", Integer.parseInt(chemistCart.getQty()));
                    line_j_object.put("UOM", chemistCart.getUOM());
                    line_j_object.put("Rate", Double.valueOf(chemistCart.getRate()));
                    line_j_object.put("Price", Double.valueOf(chemistCart.getPrice()));

                    //  String.format("%.2f", crs_cart_details.getDouble(crs_cart_details.getColumnIndex("Price")));
                    line_j_object.put("MRP", Float.valueOf(chemistCart.getMRP()));
                    line_j_object.put("Createdon", chemistCart.getCreatedon());
                    line_j_object.put("CreatedBy", Integer.parseInt(call_plan_customer_id));
                    line_j_array.put(line_j_object);
                    productarray.put(Integer.parseInt(chemistCart.getProduct_ID()));
                 //   Log.e("line_j_object", line_j_object.toString());
                }

                //  }

                main_j_array.put(main_j_object);
                main_j_array.put(line_j_array);
                main_j_array.put(productarray);

                Log.d("getpaceorderData", String.valueOf(main_j_array));

                MasterPlacedOrder masterPlacedOrder = new MasterPlacedOrder();
                masterPlacedOrder.setJson(main_j_array.toString());
                masterPlacedOrder.setCustomerID(call_plan_customer_id);
                masterPlacedOrder.setDoc_ID(Doc_Id);
                long confirm = daoSession.getMasterPlacedOrderDao().insert(masterPlacedOrder);

                if (confirm > 0) {
                    chemistCartDao.deleteInTx(chemistCartList);
                    adpter.notifyDataSetChanged();
                    Intent download_intent = new Intent(RefreshData.ACTION_CONFIRM_ORDER, null, this, RefreshData.class);
                    startService(download_intent);
                }
                //db.insert_into_chemist_order_sync(Doc_Id, main_j_array.toString(), 0);

                // db.delete_chemist_Cart(Doc_Id);
                // db.delete_chemist_Cart_Details(Doc_Id);

                order_confirmed_dialog(Doc_Id);


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void get_product_data_on_stockist() {

        _txt_customer_name.setText(getIntent().getStringExtra(CHEMIST_STOCKIST_NAME));
        client_id = pref.getString(CLIENT_ID, "");
        call_plan_customer_id = getIntent().getStringExtra(CHEMIST_STOCKIST_ID);

        if (db.check_stockist_data(call_plan_customer_id) > 0)
        {


        } else
            {

            MakeWebRequest.MakeWebRequest("get", AppConfig.GET_CHEMIST_PRODUCT_DATA,
                    AppConfig.GET_CHEMIST_PRODUCT_DATA + "[" + client_id + "," + call_plan_customer_id + "]", this, false);

        }
    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {


    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {

        try {

        if (response != null) {

         //   Log.e("Inventory",response.toString());
                if (f_name.equals(AppConfig.GET_STOCKIST_INVENTORY)) {
                    Log.e("Inventory12",response.toString());
                    new LoadData(response).execute();
                }
                if (f_name.equals(AppConfig.POST_GET_STOCKIST_LEGENDS)) {
                    Log.e("Inventory11",response.toString());
                    legend_data = response.toString();
                    get_stockist_inventory(Client_id);
                    //comment by apurva
                    get_cart_id();
                }
            }
        }
        catch (Exception e) {
         //   Log.e("Message",e.getMessage());

        }
    }

    void order_confirmed_dialog(String doc_number) {
        new AlertDialog.Builder(Create_Order_Salesman.this)
                .setTitle("Order")
                .setCancelable(false)
                .setMessage("Order placed successfully.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // new get_current_location(Create_Order_Salesman.this);

                        Bundle bundle = new Bundle();
                        bundle.putString("Order", "Placed");
                        if (resultReceiver != null) {
                            resultReceiver.send(200, bundle);
                        }

                        finish();
                    }
                }).show();
    }


    public class LoadData extends AsyncTask<Void, Void, Void> {

        JSONArray response;


        public LoadData(JSONArray response) {
            this.response = response;
        }

        //declare other objects as per your need

        @Override
        protected void onPreExecute() {
            // progressDialog = ProgressDialog.show(Create_Order.this, "Progress Dialog Title Text", "Process Description Text", true);
        }

        ;

        @Override
        protected Void doInBackground(Void... params) {

            insert_inventory_products(response);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
             progressDialog.dismiss();

          // progress.setVisibility(View.GONE);
          // get_ProductList_json(response);
            get_ProductList_json();
        }
    }

    @Override
    protected void onRestart() {
       // Log.d("Activity", "Restart");

        chemistCartList = chemistCartDao.loadAll();
        //   int previouscartValue = n_product_cart_count;

        //  n_product_cart_count = chemistCartList.size();
        if (n_product_cart_count < (n_product_cart_count = chemistCartList.size())) {
            ChemistCart chemistCart = chemistCartList.get(n_product_cart_count - 1);
            createCartBadge(n_product_cart_count);
            orderAmount = 0;
            for (int i = 0; i < chemistCartList.size(); i++) {

                orderAmount += Double.valueOf(chemistCartList.get(i).getAmount());
            }
            _OrderAmt.setText(" Rs." + String.format("%.2f", orderAmount));
        } else {
            orderAmount = 0;

            for (int i = 0; i < chemistCartList.size(); i++) {

                orderAmount += Double.valueOf(chemistCartList.get(i).getAmount());
            }
            _OrderAmt.setText(" Rs." + String.format("%.2f", orderAmount));
        }


        super.onRestart();
    }

    private void createCartBadge(int paramInt) {
        if (Build.VERSION.SDK_INT <= 15) {
            return;
        }
        if (mToolbarMenu != null) {
            MenuItem cartItem = this.mToolbarMenu.findItem(R.id.action_cart);

            if (cartItem != null) {
                LayerDrawable localLayerDrawable = (LayerDrawable) cartItem.getIcon();
                Drawable cartBadgeDrawable = localLayerDrawable
                        .findDrawableByLayerId(R.id.ic_badge);

                BadgeDrawable badgeDrawable;
                if ((cartBadgeDrawable != null)
                        && ((cartBadgeDrawable instanceof BadgeDrawable))
                        && (paramInt < 10)) {
                    badgeDrawable = (BadgeDrawable) cartBadgeDrawable;
                } else {
                    badgeDrawable = new BadgeDrawable(this);
                }
           //     Log.e("Count", String.valueOf(paramInt));
                badgeDrawable.setCount(paramInt);
                localLayerDrawable.mutate();
                localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
                cartItem.setIcon(localLayerDrawable);
            }
        }
    }


    void cancel_order(final View v) {


        if (chemistCartList.size() > 0) {

            new AlertDialog.Builder(Create_Order_Salesman.this)
                    .setTitle("Order")
                    .setMessage("Cancel Order")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            chemistCartDao.deleteInTx(chemistCartList);


                            Snackbar mySnackbar = Snackbar.make(v, "Products removed from cart", Snackbar.LENGTH_SHORT);
                            mySnackbar.show();
                            Log.d("cancelpress", "yes cancel");
//                            Intent i = new Intent(getApplicationContext(), CallPlanDetails.class);
//                            startActivity(i);
                            finish();
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                        }
                    })
                    .show();

        } else {
            OGtoast.OGtoast("Cart is empty", Create_Order_Salesman.this);
        }
    }

    public Intent getSupportParentActivityIntent() {

        Intent newIntent = null;
        try {
            if (globalVariable.getCall_plan_Started()) {
                globalVariable.setFromCustomerList(false);
                finish();
            } else {
                globalVariable.setFromCustomerList(false);
                newIntent = new Intent(Create_Order_Salesman.this, MainActivity.class);
            }
            //you need to define the class with package name

            startActivity(newIntent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        // finish();
        Intent newIntent = null;
        try {
          //  Log.d("Activity","Back to home");
            if (globalVariable.getCall_plan_Started()) {
                globalVariable.setFromCustomerList(false);
                finish();
                // newIntent = new Intent();
            } else {
                newIntent = new Intent(Create_Order_Salesman.this, MainActivity.class);
                globalVariable.setFromCustomerList(false);
            }
            //you need to define the class with package name

            //  daoSession.getStockistProductsDao().deleteAll();
            startActivity(newIntent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void insert_inventory_products(JSONArray response) {

       /* String jsondata = response.toString();
        GsonBuilder builder = new GsonBuilder();
        Gson mGson = builder.create();
        Type listType = new TypeToken<List<StockistProducts>>() {
        }.getType();
        posts = mGson.fromJson(jsondata, listType);*/

        ArrayList<StockistProducts> posts = new ArrayList<StockistProducts>();


        JSONObject j_obj;
        StockistProducts stockistProducts;
        try {


            for (int i = 0; i < response.length(); i++) {
                j_obj = response.getJSONObject(i);

                stockistProducts = new StockistProducts();
                stockistProducts.setProduct_ID(j_obj.getString("Product_ID"));
                stockistProducts.setItemcode(j_obj.getString("Itemcode"));
                stockistProducts.setItemname(j_obj.getString("Itemname"));
                stockistProducts.setPacksize(j_obj.getString("Packsize"));
                stockistProducts.setMRP(j_obj.getJSONArray("line_items").getJSONObject(0).getString("Mrp"));
                stockistProducts.setRate(j_obj.getJSONArray("line_items").getJSONObject(0).getString("Rate"));
                stockistProducts.setStock(j_obj.getString("Stock"));
                stockistProducts.setMfgCode(j_obj.getJSONArray("line_items").getJSONObject(0).getString("MfgCode"));
                stockistProducts.setMfgName(j_obj.getJSONArray("line_items").getJSONObject(0).getString("MfgName"));
                stockistProducts.setDoseForm(j_obj.getString("DoseForm"));
                stockistProducts.setScheme(j_obj.getString("Scheme"));
               //add flag  to show scheme if set
              //  stockistProducts.setSalesman(j_obj.getString("salesman"));
                posts.add(stockistProducts);

            }

        } catch (Exception e) {
            e.printStackTrace();

        }
       // Log.e("Post",posts.toString());
        daoSession.getStockistProductsDao().deleteAll();
        daoSession.getStockistProductsDao().insertInTx(posts,true);
        //change by apurva
        sreach_product_list=posts;
      //  Log.e("Responsee",response.toString());


    }


    private void get_stockist_inventory(String Client_id) {

        Log.d("cliendid",client_id);

        //progress.setVisibility(View.VISIBLE);
        MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_INVENTORY,
                AppConfig.GET_STOCKIST_INVENTORY + Client_id, this, false);

    }

    void show_inventory()
    {
        Intent i = new Intent(Create_Order_Salesman.this, InventorylistActivity.class);
        i.putExtra("client_id", call_plan_customer_id);
        i.putExtra("order_amount", orderAmount);
        startActivity(i);
    }

    void delete_product_from_cart(ChemistCart chemistCart) {

        chemistCartDao.delete(chemistCart);
        chemistCartList.remove(chemistCart);

        int item_count = chemistCartList.size();
        float orderAmounts = .2f;

        for (ChemistCart cart : chemistCartList) {

            orderAmounts += Float.valueOf(cart.getAmount());

        }
        orderAmount = orderAmounts;
        /*Log.e("itme_no", itme_no);
        db.delete_product_from_cart_chemist_Cart_Details(createdOn);

        price = db.get_total_order_amount(Doc_Id) - price;*/
        // Integer item_count = (db.get_total_order_item_count(Doc_Id));
        // _OrderAmt.setText(" Rs." + String.valueOf(orderAmounts));
        _OrderAmt.setText(" Rs." + String.format("%.2f", orderAmounts));
        // db.update_into_chemist_cart(Doc_Id, item_count, price.toString());

        if (chemistCartList.size() == 0) {

            Cart_Id_available = false;
            _OrderAmt.setText("");
            orderAmount = 0;
        }

        if (mToolbarMenu != null)
        {
            createCartBadge(item_count);
        }

        OGtoast.OGtoast("Product removed succesfully", Create_Order_Salesman.this);

        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
        {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        adpter.notifyDataSetChanged();
    }


    void delete_product_from_cart(String itme_no, Float price) {
      //  Log.e("itme_no", itme_no);
        db.delete_product_from_cart_chemist_Cart_Details(Doc_Id, itme_no);

        price = db.get_total_order_amount(Doc_Id) - price;
        Integer item_count = (db.get_total_order_item_count(Doc_Id)) - 1;
        _OrderAmt.setText(" Rs." + price.toString());
        db.update_into_chemist_cart(Doc_Id, item_count, price.toString());

        if (item_count == 0)
        {
            db.delete_chemist_Cart(Doc_Id);
        }

        if (mToolbarMenu != null) {
            createCartBadge(item_count);
        }

        OGtoast.OGtoast("Product removed succesfully", Create_Order_Salesman.this);

        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    void get_stockist_legends() {
        try {

            JSONArray j_arr = new JSONArray();
            j_arr.put(client_id);

            Log.d("printjarray", String.valueOf(j_arr));
            progressDialog = ProgressDialog.show(Create_Order_Salesman.this, "Please Wait", "Loading Products.... ", true);
            MakeWebRequest.MakeWebRequest("out_array", AppConfig.POST_GET_STOCKIST_LEGENDS, AppConfig.POST_GET_STOCKIST_LEGENDS,
                    j_arr, this, true, "");

        } catch (Exception e)
        {

        }
    }

    void set_stock_color_legend(Integer Stock) {
        try {
            JSONArray j_arr = new JSONArray(legend_data);
            for (int i = 0; i < j_arr.length(); i++) {
                JSONObject j_ob = j_arr.getJSONObject(i);

                if (Stock >= j_ob.getInt("StartRange") &&
                        Stock <= j_ob.getInt("EndRange")) {
                    String color_code = j_ob.getString("ColorCode");
                    legend_mode=j_ob.getString("Legend_Mode");

                    legendName =j_ob.getString("LegendName");
                   // Log.d("legendNameThirty", legendName);


                    if (legend_mode.equals("3"))
                    {
                        stock.setBackgroundColor(Color.parseColor(color_code));
                        stock.setText(legendName);
                    }
                    else if(legend_mode.equals("2"))
                    {
                        stock.setBackgroundColor(Color.parseColor(color_code));
                        stock.setText(String.valueOf(Stock));
                        stock.setTextColor(getResources().getColor(R.color.white));
                    }
                    else if (legend_mode.equals("1"))
                    {
                        stock.setBackgroundColor(Color.parseColor(color_code));
                    }
                    //stock.setBackgroundColor(Color.parseColor(color_code));




                    // stock.getBackground().setColorFilter(Color.parseColor(color_code), PorterDuff.Mode.MULTIPLY);
                }
            }

        } catch (Exception e) {

            e.toString();

        }
    }


    @Override
    protected void onDestroy() {
       // Log.d("Activity", "onDestroy");
        //  daoSession.getStockistProductsDao().deleteAll();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
      //  Log.d("Activity", "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {

      //  Log.d("Activity", "onStop");
        super.onStop();
    }
}
