package com.synergy.ordergenie.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.OGtoast;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener;

import static com.synergy.ordergenie.R.id.Cheque_date;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class Payment_Option extends AppCompatActivity implements MakeWebRequest.OnResponseSuccess, View.OnClickListener {

    /*   private SearchView searchView;
       private Menu oMenu;*/
    private Boolean IsPaymentoptionSelected = false;
    private Boolean IsChequeSelected = false;

    String chemist_Id;
    StringBuffer date_buff;
    //private int mYear, mMonth, mDay;
    DatePickerDialog datePickerDialog;


    private int mYear, mMonth, mDay;
    ResultReceiver resultReceiver;
    private SharedPreferences pref;
    private Integer payment_id;

    LinearLayout PartPaymentOption;
    @BindView(R.id.toolbar)
    Toolbar _toolbar;

    @BindView(R.id.lblinvoiceid)
    TextView lblinvoiceid;

    @BindView(R.id.invoiceId)
    TextView invoiceId;

    @BindView(R.id.lblorderDate)
    TextView _lblorderDate;

    @BindView(R.id.orderDate)
    TextView _orderDate;

    @BindView(R.id.lblTotalItem)
    TextView _lblTotalItem;

    @BindView(R.id.TotalItem)
    TextView _TotalItem;

    @BindView(R.id.lblInvAmt)
    TextView _lblInvAmt;

    @BindView(R.id.InvAmt)
    TextView _InvAmt;

    @BindView(R.id.lblBalAmt)
    TextView _lblBalAmt;

    @BindView(R.id.BalAmt)
    TextView _BalAmt;

    @BindView(R.id.checkPartpay)
    CheckBox _checkPartpay;

    @BindView(R.id.note)
    TextView _note;

    @BindView(R.id.desireAmt)
    EditText _desireAmt;

    @BindView(R.id.spinner)
    Spinner _SelectPayOption;

    @BindView(R.id.ComPay)
    Button _ComPay;


    @BindView(Cheque_date)
    TextView _Cheque_date;

    @BindView(R.id.Cheque_number)
    EditText _Cheque_number;

    @BindView(R.id.micr_number)
    EditText _micr_number;


    @BindView(R.id.account_number)
    EditText _account_number;

    @BindView(R.id.bank_name)
    AutoCompleteTextView bank_name;

    String[] comp_names;

    private Date current_date = Calendar.getInstance().getTime();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    // private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private SimpleDateFormat sdYear = new SimpleDateFormat("yyyy");
    private SimpleDateFormat sdMonth = new SimpleDateFormat("MM");
    private SimpleDateFormat sdDay = new SimpleDateFormat("dd");
    private int nYear, nMonth, Nday;
    String chemist_id;

    @OnItemSelected(R.id.spinner)
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment__option);


        ButterKnife.bind(this);


        SharedPreferences prefs = getSharedPreferences("MY PREF", MODE_PRIVATE);
        chemist_id = prefs.getString("chemist_id", null);
        // Log.d("printpaymentId",chemist_id);

        comp_names = getResources().getStringArray(R.array.Company_names_list);
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        setSupportActionBar(_toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle("Payment Details");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }
        ArrayAdapter<String> compny_names_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, comp_names);
        bank_name.setThreshold(1);
        bank_name.setAdapter(compny_names_adapter);

        PartPaymentOption = (LinearLayout) findViewById(R.id.PartPayment);
        PartPaymentOption.setVisibility(View.GONE);
        resultReceiver = getIntent().getParcelableExtra("receiver");

        invoiceId.setText(getIntent().getStringExtra("Invoiceno"));


        _Cheque_date.setOnClickListener(this);

        bank_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    bank_name.showDropDown();
                }
            }
        });
        bank_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(bank_name.getWindowToken(), 0);

            }
        });

        try {

            String date_Length = sdf.format(dateFormat.parse(getIntent().getStringExtra("Invoicedate")));
           /* _orderDate.setText(sdf.format(dateFormat.parse(getIntent().getStringExtra("Invoicedate"))));*/
            date_buff = new StringBuffer(date_Length);
            date_buff.setLength(10);
            //  Log.d("date_length", String.valueOf(date_buff));
            _orderDate.setText(date_buff);

        } catch (Exception e) {

        }

        _TotalItem.setText("" + getIntent().getIntExtra("Totalitems", 0));
        _InvAmt.setText("Rs. " + getIntent().getStringExtra("Billamount"));
        _BalAmt.setText("" + getIntent().getStringExtra("Balanceamt"));

        if (Integer.parseInt(getIntent().getStringExtra("Billamount")) >= 500) {
            PartPaymentOption.setVisibility(View.VISIBLE);
        }
        _checkPartpay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    _desireAmt.setVisibility(View.VISIBLE);
                } else {
                    _desireAmt.setVisibility(View.GONE);
                }
            }
        });

        _SelectPayOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    IsPaymentoptionSelected = false;
                    _Cheque_date.setVisibility(View.GONE);
                    _Cheque_number.setVisibility(View.GONE);
                    _account_number.setVisibility(View.GONE);
                    bank_name.setVisibility(View.GONE);
                    _micr_number.setVisibility(View.GONE);
                } else {
                    IsPaymentoptionSelected = true;

                    _Cheque_date.setVisibility(View.GONE);
                    _Cheque_number.setVisibility(View.GONE);
                    _account_number.setVisibility(View.GONE);
                    bank_name.setVisibility(View.GONE);
                    _micr_number.setVisibility(View.GONE);
                }
                if (position == 2) {

                    _Cheque_date.setVisibility(View.VISIBLE);
                    _Cheque_number.setVisibility(View.VISIBLE);

                    /*comment Account num visiblity gone not need for check payment for said Rashid sir*/
                    _account_number.setVisibility(View.GONE);
                    bank_name.setVisibility(View.VISIBLE);
                    _micr_number.setVisibility(View.VISIBLE);


                    payment_id = 3;
                    IsChequeSelected = true;
                } else {
                    payment_id = 2;
                    IsChequeSelected = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @OnClick(R.id.ComPay)
    void onclick_submit() {

        if (Validate()) {
            save_payment();
        }

    }


    Boolean Validate() {
        Boolean success = true;

        if (_checkPartpay.isChecked()) {
            if (_desireAmt.getText().toString().isEmpty()) {
                success = false;
                OGtoast.OGtoast("Please enter the amount for part payment", Payment_Option.this);
            }
        }
        if (!IsPaymentoptionSelected) {
            success = false;
            OGtoast.OGtoast("Please select a payment option", Payment_Option.this);
        }
        if (IsChequeSelected) {
            if (_Cheque_date.getText().toString().isEmpty()) {
                success = false;
                OGtoast.OGtoast("Please enter Cheque date", Payment_Option.this);
            } else if (_Cheque_number.getText().toString().isEmpty()) {
                success = false;
                OGtoast.OGtoast("Please enter Cheque Number", Payment_Option.this);
            } /*else if (_account_number.getText().toString().isEmpty()) {
                success = false;
                OGtoast.OGtoast("Please enter Account Number", Payment_Option.this);
            } */ else if (bank_name.getText().toString().isEmpty()) {
                success = false;
                OGtoast.OGtoast("Please enter a Bank Name", Payment_Option.this);
            }/*else if (_micr_number.getText().toString().isEmpty()) {
                success = false;
                OGtoast.OGtoast("Please enter MICR Number", Payment_Option.this);
            }*/
           /* if(_account_number.getText().toString().isEmpty())
            {
                success=false;
                OGtoast.OGtoast("Please enter Account Number",Payment_Option.this);
            }*/
        }


        return success;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_payment_options, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
            case R.id.action_search:

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void save_payment() {

        try {
            String amount = "";

            JSONObject j_obj = new JSONObject();
            String Narration_Text = "";

            j_obj.put("Invoice_No", invoiceId.getText().toString());
            j_obj.put("Doc_No", "");
            j_obj.put("Doc_Date", dateFormat.format(Calendar.getInstance().getTime()));
            j_obj.put("PaymodeId", payment_id);
            j_obj.put("PaymodeDesc", _SelectPayOption.getSelectedItem().toString());

            if (_checkPartpay.isChecked()) {
                amount = _desireAmt.getText().toString();
            } else {
                amount = _BalAmt.getText().toString();

            }


            if (IsChequeSelected) {
                j_obj.put("Chq_No", _Cheque_number.getText().toString());
                j_obj.put("Chq_Date", _Cheque_date.getText().toString());
                j_obj.put("BankAccountID", _account_number.getText().toString());
                j_obj.put("Bank_Id", "");
                j_obj.put("BankName", bank_name.getText().toString());

                Narration_Text = " Cheque Payment- Ref No- " + invoiceId.getText().toString() + " and Cheque No " + " Dated - " + sdf.format(Calendar.getInstance().getTime())
                        + " of RS." + amount + "";

            } else {
                j_obj.put("Chq_No", "");
                j_obj.put("Chq_Date", "");
                j_obj.put("BankAccountID", "0");
                j_obj.put("Bank_Id", "");
                j_obj.put("BankName", "");


                Narration_Text = "Cash Payment- Ref No- " + invoiceId.getText().toString() + " Dated - " + sdf.format(Calendar.getInstance().getTime())
                        + " of RS." + amount + "";


            }

            j_obj.put("Amount", amount);
            j_obj.put("Narration", Narration_Text);

            Random rand = new Random();
            int randomNumber = rand.nextInt(1000) + 1;
            //   Log.d("randomnumer", String.valueOf(randomNumber));
            j_obj.put("PaymentID", randomNumber);
            j_obj.put("Stockist_Client_ID", pref.getString(CLIENT_ID, "0"));

            j_obj.put("Chemist_ID", chemist_id);
            Log.d("RESPONCEPAYMENT13", String.valueOf(j_obj));
            MakeWebRequest.MakeWebRequest("Post", AppConfig.SAVE_PAYMENT, AppConfig.SAVE_PAYMENT, j_obj, this, true);

        } catch (Exception e) {
            e.toString();
        }
    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {


        // Log.d("RESPONCEPAYMENT11", f_name);
        // Log.d("RESPONCEPAYMENT12", String.valueOf(response));
        if (response != null) {

            if (f_name.equals(AppConfig.SAVE_ORDER_INVOICE)) {
                new AlertDialog.Builder(Payment_Option.this)
                        .setTitle("Payment")
                        .setCancelable(false)
                        .setMessage("Payment made successfully.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                Bundle bundle = new Bundle();
                                bundle.putString("Payment", "Done");
                                if (resultReceiver != null) {
                                    resultReceiver.send(300, bundle);
                                }


                                finish();
                            }
                        }).show();
            }
        }
    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {
        try {


            if (response != null) {
                Log.e("response", response.toString());
                if (f_name.equals(AppConfig.SAVE_PAYMENT)) {
                    save_invoice(invoiceId.getText().toString(), "4");
                }
            }
        } catch (Exception e) {
            e.toString();
        }

    }


    void save_invoice(String Invoice_id, String status) {

        try {


            JSONObject main_j_obj = new JSONObject();
            JSONArray j_array = new JSONArray();

            JSONObject j_obj = new JSONObject();

            j_obj.put("InvoiceNo", Invoice_id);
            j_obj.put("StockistID", pref.getString(CLIENT_ID, "0"));
            j_obj.put("Status", status);

            j_array.put(j_obj);

            main_j_obj.put("invoices", j_array);
            //   Log.e("MAin",main_j_obj.toString());

            // MakeWebRequest.MakeWebRequest("Post", AppConfig.POST_CHEMIST_CONFIRM_ORDER, AppConfig.POST_CHEMIST_CONFIRM_ORDER,
            //       new JSONArray().put(main_j_obj), this,false,"");


            MakeWebRequest.MakeWebRequest("out_array", AppConfig.SAVE_ORDER_INVOICE, AppConfig.SAVE_ORDER_INVOICE,
                    null, this, true, main_j_obj.toString());

            //  MakeWebRequest.MakeWebRequest("Post", AppConfig.SAVE_ORDER_INVOICE, AppConfig.SAVE_ORDER_INVOICE, main_j_obj, this, true);


        } catch (Exception e) {

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.Cheque_date:

                show_Date_Picker();

                break;
        }


    }

    private void show_Date_Picker() {

        //    Log.d("shw","yaya");

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(this,
                new android.app.DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        _Cheque_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();


    }
}
