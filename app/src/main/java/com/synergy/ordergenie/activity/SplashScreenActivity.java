package com.synergy.ordergenie.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.utils.SessionManager;


public class SplashScreenActivity extends AppCompatActivity {

	private static final String TAG = "SplashScreenActivity";
	private final int SPLASH_DISPLAY_LENGTH = 5000;

	private SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.splash_screen);
		session = new SessionManager(getApplicationContext());

		//if (isIMEIPermissionGranted() == true) {

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (session.isLoggedIn()) {
					showmainscreen();
				}else {
					showloginscreen();
				}
			}
		}, SPLASH_DISPLAY_LENGTH);
		//}
	}


	private void showmainscreen() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}

	private void showloginscreen() {
		Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
		startActivity(intent);
		finish();
	}




}
