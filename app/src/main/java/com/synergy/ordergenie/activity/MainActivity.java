package com.synergy.ordergenie.activity;

import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.synergy.ordergenie.fragment.DistributorDashboardFragment;
import com.synergy.ordergenie.utils.get_current_location_ondashboard;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;
import com.synergy.ordergenie.fragment.ChemistDashboardFragment;
import com.synergy.ordergenie.fragment.DashboarddetailsmenuFragment;
import com.synergy.ordergenie.fragment.StockistDashboardFragment;
import com.synergy.ordergenie.utils.ConstData;
import com.synergy.ordergenie.utils.MakeWebRequest;
import com.synergy.ordergenie.utils.RefreshData;
import com.synergy.ordergenie.utils.SessionManager;
import com.synergy.ordergenie.utils.TypefaceUtil;
import com.synergy.ordergenie.utils.get_current_location;

import static com.synergy.ordergenie.app.AppConfig.GET_STOCKIST_STASTICS;
import static com.synergy.ordergenie.utils.ConstData.Login_type.CHEMIST;
import static com.synergy.ordergenie.utils.ConstData.Login_type.STOCKIST;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_LATITUDE;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_LOGIN_NAME;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_LONGITUDE;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_NAME;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_PROFILE_IMAGE;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class MainActivity extends AppCompatActivity implements StockistDashboardFragment.OnmenuitemSelected,ChemistDashboardFragment.OnmenuitemSelected, MakeWebRequest.OnResponseSuccess,DistributorDashboardFragment.OnmenuitemSelected, DashboarddetailsmenuFragment.OnFragmentInteractionListener{


    RelativeLayout relativeLayout;
    String Latitude, Longitude, CurrentLocation;
    public static String User_name;
    private String login_type;
    private Fragment o_DashboardFragment = null;
    DashboarddetailsmenuFragment o_DashboarddetailsmenuFragment;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    DisplayImageOptions options;
    private Date current_date = Calendar.getInstance().getTime();
    public TextView tx_customercount_value;
    public TextView tx_pendingbills_value;
    public TextView tx_orders_value;
    public TextView tx_sales_value;
    public TextView tx_returns_value;
    public TextView tx_inv_value;
    public static String text_int_total_vallue = "";
    public TextView _txt_profile_name;
    public TextView _txt_profile_email;
    private ImageView iv_profile_pic;
    private static final int NOTIFICATION_ID = 147894;
    private List<Map<String,String >> list_data=new ArrayList<Map<String,String >>();
    public static final String CHEMIST_IS_LAST_10_ORDER = "chemist_is_last_10_orders";

    public static String last_Ten_orders;

    //Stockist Navigation
    public TextView _menu_logout,_menu_changepassword,txt_cltlegealname;

    private String User_id, Stockist_id,isDummypass, cLegalName, cLoginName;;

    SharedPreferences pref;
    AppController globalVariable;

    @BindView(R.id.toolbar)
    Toolbar _toolbar;

    @BindView(R.id.appbar)
    AppBarLayout _appbar;

    @BindView(R.id.main_content)
    CoordinatorLayout _mainContent;

    @BindView(R.id.drawer)
    DrawerLayout _drawer;

    @BindView(R.id.nav_view)
    NavigationView _navView;

   /* @BindView(R.id.pbr_today_task)
    ProgressBar pbr_today_task;*/


/*    @OnClick(R.id.menu_logout)
    void menu_logout() {
        showloginscreen();
    }*/


  /*  @OnClick(R.id.lnr_nav_header)
    void edt_profile() {
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
    }*/

   /* @OnClick(R.id.lnr_nav_header_c)
    void edt_profile_c() {
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
    }*/

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        //txt_cltlegealname = (TextView) findViewById(R.id.txt_cltlegealname);

        //txt_cltlegealname.setText("Harish badgujar");

        //globalVariable = (AppController) getApplicationContext();
//        jsonParams.put("UserID", pref.getString(USER_ID, "0"));
//        jsonParams.put("Latitude", String.valueOf(location.getLatitude()));
//        jsonParams.put("Longitude", String.valueOf(location.getLongitude()));
//        jsonParams.put("CurrentLocation", adress);
//        userid =  pref.getString(USER_ID, "0");
//       Latitude= String.valueOf(location.getLatitude());
//        Longitude= String.valueOf(location.getLongitude());
//        CurrentLocation= adress;

//Lato-Regular
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        ButterKnife.bind(this);
        options = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(1000)) //rounded corner bitmap
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        initImageLoader();
        globalVariable = (AppController) getApplicationContext();
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        session = new SessionManager(getApplicationContext());
        setSupportActionBar(_toolbar);

        //  pbr_today_task.setProgress(50);

        cLoginName = pref.getString(CLIENT_LOGIN_NAME, "0");
        cLegalName = pref.getString(CLIENT_NAME, "0");
        User_id = pref.getString(USER_ID, "0");
        Stockist_id = pref.getString(CLIENT_ID, "0");
//        isDummypass=pref.getString(CLIENT_IS_DUMMY_PASS, "0");
//        Log.d("isDummypass",isDummypass);
//        if (isDummypass.equals("1"))
//        {
//            Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
//            startActivity(intent);
//        }

        ActionBar supportActionBar = getSupportActionBar();
        call_refresh_data();

		/*_navView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
			public void onClick(View v)
			{

			}
		});*/

        if (supportActionBar != null) {
            //	VectorDrawableCompat indicator =
                    /*VectorDrawableCompat.create(getResources(), R.drawable.ic_menu, getTheme());
            indicator.setTint(ResourcesCompat.getColor(getResources(), R.color.white, getTheme()));*/
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        set_navigation_drawer();
        get_client_details();
        create_dashboard();
       // int appVersion = PreferenceUtil.getAppVersion(getApplicationContext());
       // Log.e("versioncode",String.valueOf(appVersion));
      //  if(appVersion != 0 && appVersion > BuildConfig.VERSION_CODE){
      //updateAppMess  UpdateDialog();
        //  if (login_type.equals(CHEMIST) && (pref.getString(CLIENT_LATITUDE, "0")!=("")||pref.getString(CLIENT_LONGITUDE, "0")!=("")||pref.getString(CLIENT_LATITUDE, "0")!=null||pref.getString(CLIENT_LONGITUDE, "0")!=null))

 if (login_type.equals(CHEMIST) && (pref.getString(CLIENT_LATITUDE, "0").equals("")||pref.getString(CLIENT_LONGITUDE, "0").equals("")||pref.getString(CLIENT_LATITUDE, "0")==null||pref.getString(CLIENT_LONGITUDE, "0")==null))

   // if (login_type.equals(CHEMIST))
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(getApplicationContext().getString(R.string.update_text));
            alertDialogBuilder.setMessage(getApplicationContext().getString(R.string.update_loc_text) + " "+"?");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("Save Location", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id)
                {
                    UpdateDialog();
                }
            });
            alertDialogBuilder.setNegativeButton("No,Thanks", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });

            alertDialogBuilder.show();

    }


//       else if (login_type.equals(CHEMIST) && pref.getString(CLIENT_LATITUDE, "0")!="") {
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//            alertDialogBuilder.setTitle(getApplicationContext().getString(R.string.update_text));
//            alertDialogBuilder.setMessage(getApplicationContext().getString(R.string.update_loc_text) + " "+"?");
//            alertDialogBuilder.setCancelable(false);
//            alertDialogBuilder.setPositiveButton("Save Location", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id)
//                {
//                    UpdateDialog();
//                }
//            });
//
//            alertDialogBuilder.setNegativeButton("No,Thanks", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    // Write your code here to invoke NO event
//                    dialog.cancel();
//                }
//            });
//
//            alertDialogBuilder.show();
//
//        }
    }
    //*********************************Are you at your store***************************************//
//    private void updateAppMessage() {
//        Snackbar appUpdateSnackbar = Snackbar.make(findViewById(R.id.drawer_layout),
//                R.string.update_app_text, Snackbar.LENGTH_SHORT);
//        appUpdateSnackbar.setAction(R.string.update_text, new AppUpdateListener());
//        appUpdateSnackbar.show();
//    }
//    public class AppUpdateListener implements View.OnClickListener{
//
//        @Override
//        public void onClick(View v) {
//            Intent intent = new Intent(Intent.ACTION_VIEW);
//            intent.setData(Uri.parse("market://details?id=com.chemistproject.smartp"));
//            try{
//                startActivity(intent);
//            }catch(Exception e){
//
//                // intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.android.app"));
//                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.chemistproject.smartp"));
//            }
//        }
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_notfic) {
            Intent i=new Intent(this,DistributorNotificationActivity.class);
            startActivity(i);
            return true;
        }

       /* if (id == R.id.action_export_db) {
            exportDB();
            return true;
        }
        if (id == R.id.action_logout) {
            showloginscreen();
            return true;
        } */
        else if (id == android.R.id.home) {
            _drawer.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login_type.equals(STOCKIST))
        {
            getMenuInflater().inflate(R.menu.menu_distributor_dashboard, menu);
          //  getMenuInflater().inflate(R.layout.menu_distributor_dashboard, menu);

            RelativeLayout rLayout = (RelativeLayout) menu.findItem(R.id.action_notfic).getActionView();

            TextView txt_cltlegealname = (TextView) rLayout.findViewById(R.id.txt_compny);
            TextView txt_username = (TextView) rLayout.findViewById(R.id.txt_username);

            txt_username.setText(cLoginName);
            txt_cltlegealname.setText(cLegalName);

        }else
        {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }

        return true;
    }

    private void create_dashboard() {

        login_type = pref.getString(ConstData.user_info.CLIENT_ROLE, "");

        if (login_type != null) {

            if (login_type.equals(STOCKIST)) {
                MakeWebRequest.MakeWebRequest("get", AppConfig.GET_USER_OFFERS,
                        AppConfig.GET_USER_OFFERS + Stockist_id, this, true);
             //   o_DashboardFragment = DistributorDashboa
                // rdTab.newInstance();
              o_DashboardFragment = DistributorDashboardFragment.newInstance();
//                Intent intent = new Intent(MainActivity.this, DistributorDashboardTab.class);
//                startActivity(intent);
            }else if(login_type.equals(CHEMIST)) {
//                MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_STASTICS,
//                        AppConfig.GET_STOCKIST_STASTICS + "[" + User_id + "," + Stockist_id + "]", this, true);
                MakeWebRequest.MakeWebRequest("get", AppConfig.GET_USER_OFFERS,
                        AppConfig.GET_USER_OFFERS + Stockist_id, this, true);
                o_DashboardFragment = ChemistDashboardFragment.newInstance();
            }
            else {

              // String dayno=get_day_number(current_date);
             // Log.d("dayno",dayno);
                MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_STASTICS,
                        AppConfig.GET_STOCKIST_STASTICS + "[" + Stockist_id + "," + User_id + "]", this, true);
                o_DashboardFragment = StockistDashboardFragment.newInstance();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction  fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_centerscreen, o_DashboardFragment);
            fragmentTransaction.commit();
        }
    }

    private void change_pass_dialog() {

        new AlertDialog.Builder(this)
                .setTitle("Change Password")
                .setMessage("Change your passowrd")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override

                    public void onClick(DialogInterface dialog, int which) {
                        //  System.exit(0);
                        dialog.dismiss();
                        Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();

    }
    private void set_navigation_drawer() {

        login_type = pref.getString(ConstData.user_info.CLIENT_ROLE, "");

        if (login_type != null) {
            if (login_type.equals(CHEMIST)) {
                LayoutInflater inflater = LayoutInflater.from(this);
                final View dialogview = inflater.inflate(R.layout.navheader_chemist, null);
                _txt_profile_name = (TextView) dialogview.findViewById(R.id.txt_profile_name_c);
                _txt_profile_email = (TextView) dialogview.findViewById(R.id.txt_profile_email_c);
                iv_profile_pic = (ImageView) dialogview.findViewById(R.id.iv_profile_pic);
                imageLoader.displayImage(pref.getString(CLIENT_PROFILE_IMAGE, ""), iv_profile_pic, options);
                _navView.inflateMenu(R.menu.menu_nv_chemist);
                _navView.addHeaderView(dialogview);
                _navView.setBackground(getResources().getDrawable(R.drawable.white_bg_border));

                _navView.setNavigationItemSelectedListener(
                        new NavigationView.OnNavigationItemSelectedListener() {
                            // This method will trigger on item Click of navigation menu
                            @Override
                            public boolean onNavigationItemSelected(MenuItem menuItem) {
                                // Set item in checked state
                                menuItem.setChecked(true);

                                int id = menuItem.getItemId();
                                if (id == R.id.Chemist_LogOut) {
                                    showloginscreen();
                                    return true;
                                }
                                if (id == R.id.Chemist_Lastorders) {
                                    showLastOrders();
                                    return true;
                                }
                                if (id == R.id.Chemist_Messages) {
                                    showMessageScreen();
                                    return true;
                                }
                                if (id == R.id.Chemist_MyProfile) {
                                    showedit();
                                    return true;
                                }
                                if (id == R.id.Chemist_location) {
                                    globalVariable.setFromMenuItemClick(true);
                                  new get_current_location(MainActivity.this);

                                   // location_alert();
                                    return true;
                                }
                                if (id == R.id.Chemist_ChangePassword) {
                                    showchangepassword();
                                    return true;
                                }
                                // TODO: handle navigation
                                // Closing drawer on item click
                                _drawer.closeDrawers();
                                return true;
                            }
                        });
            }
            //stockist login
           else if (login_type.equals(STOCKIST)) {
                LayoutInflater inflater = LayoutInflater.from(this);
                final View dialogview = inflater.inflate(R.layout.navheader_stockist, null);
                _txt_profile_name = (TextView) dialogview.findViewById(R.id.txt_profile_name_c);
                _txt_profile_email = (TextView) dialogview.findViewById(R.id.txt_profile_email_c);
                iv_profile_pic = (ImageView) dialogview.findViewById(R.id.iv_profile_pic);
                imageLoader.displayImage(pref.getString(CLIENT_PROFILE_IMAGE, ""), iv_profile_pic, options);
                _navView.inflateMenu(R.menu.menu_nv_stockist);
                _navView.addHeaderView(dialogview);
                _navView.setNavigationItemSelectedListener(
                        new NavigationView.OnNavigationItemSelectedListener() {
                            // This method will trigger on item Click of navigation menu
                            @Override
                            public boolean onNavigationItemSelected(MenuItem menuItem) {
                                // Set item in checked state
                                menuItem.setChecked(true);
                                int id = menuItem.getItemId();
                                if (id == R.id.customers) {
                                   showDistributorcustomer();
                                    return true;
                                }
                                if (id == R.id.catalog) {
                                    showDistributorProductCatlog();
                                    return true;
                                }
                                if (id == R.id.orders) {
                                    showDistributorOrders();
                                    return true;
                                }
                                if (id == R.id.pendingbill) {
                                    showDistributorPendingBill();
                                    return true;
                                }
                                if (id == R.id.sales) {
                                  showDistributorSalesandReturn();
                                    return true;
                                }
                                if(id == R.id.users){

                                    show_Users();

                                    return true;
                                }
                                if(id == R.id.notifications){

                                    //Toast.makeText(MainActivity.this,"Show Notifications Here",Toast.LENGTH_SHORT).show();
                                    show_Notifications();

                                    return true;
                                }
                                if (id == R.id.callplan) {
                                    showDistributorCallPlan();
                                    return true;
                                }

                                if (id == R.id.sto_payment) {
                                    showDistributorPayments();
                                    return true;
                                }
                                if (id == R.id.track) {
                                    showDistributorTrackUser();
                                    return true;
                                }
                                if (id == R.id.notification) {
                                    showDistributorNotification();
                                    return true;
                                }
                                if (id == R.id.livechat) {
                                   // showDistributorLiveChat();
                                    return true;
                                }
                                if (id == R.id.LogOut) {
                                    showloginscreen();
                                    return true;
                                }
                                // TODO: handle navigation
                                // Closing drawer on item click
                                _drawer.closeDrawers();
                                return true;
                            }
                        });
            }
            else {
                LayoutInflater inflater = LayoutInflater.from(this);
                final View dialogview = inflater.inflate(R.layout.navigation_stockist, null);
                _txt_profile_name = (TextView) dialogview.findViewById(R.id.txt_profile_name);
                _txt_profile_email = (TextView) dialogview.findViewById(R.id.txt_profile_email);
                _menu_logout = (TextView) dialogview.findViewById(R.id.menu_logout);
                _menu_changepassword = (TextView) dialogview.findViewById(R.id.menu_changepassword);
                TextView _menu_myprofile = (TextView) dialogview.findViewById(R.id.menu_profile);
                TextView _menu_messages = (TextView) dialogview.findViewById(R.id.menu_messages);
                TextView _menu_location = (TextView) dialogview.findViewById(R.id.menu_location);
                TextView _menu_last10orders = (TextView) dialogview.findViewById(R.id.menu_last10_orders);
                iv_profile_pic = (ImageView) dialogview.findViewById(R.id.iv_profile_pic);

                imageLoader.displayImage(pref.getString(CLIENT_PROFILE_IMAGE, ""), iv_profile_pic, options);
                _navView.addView(dialogview);
                //_menu_logout Button click Events
                _menu_logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //showloginscreen();
                        showlogin_delete_saveddata();
                        // Closing drawer on item click
                        _drawer.closeDrawers();
                    }
                });
                _menu_myprofile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //
                        showedit();
                        // Closing drawer on item click
                        _drawer.closeDrawers();
                    }
                });
                _menu_changepassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //
                        showchangepassword();
                        // Closing drawer on item click
                        _drawer.closeDrawers();
                    }
                });
                _menu_last10orders.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        showLastOrdersSalesMan();
                        // Closing drawer on item click
                        _drawer.closeDrawers();
                    }
                });

                _menu_messages.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //
                        showMessageScreen();
                        // Closing drawer on item click
                        _drawer.closeDrawers();
                    }
                });

                _menu_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //
                        globalVariable.setFromMenuItemClick(true);
                        new get_current_location(MainActivity.this);
                        // Closing drawer on item click
                        _drawer.closeDrawers();
                    }
                });
            }
        }
    }

    private void show_Notifications() {

        //Intent intent = new Intent(MainActivity.this, SelectedpaymentList.class);
        Intent intent = new Intent(this,DistributorNotificationActivity.class);
        startActivity(intent);

    }

    private void location_alert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getApplicationContext().getString(R.string.update_text));
        alertDialogBuilder.setMessage(getApplicationContext().getString(R.string.update_loc_text) + " ");
       // alertDialogBuilder.setMessage(getApplicationContext().getString(R.string.update_loc_text) + get_current_location_ondashboard(LocationAddress.getAddressFromLocation(get_current_location_ondashboard..getLatitude(), location.getLongitude())));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id)
            {
                dialog.cancel();
            }
        });
//        alertDialogBuilder.setNegativeButton("No,Thanks", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                // Write your code here to invoke NO event
//                dialog.cancel();
//            }
//        });

        alertDialogBuilder.show();
    }


    @Override
    public void OnmenuitemSelected(int imageResId, String name, String description, String url) {
        //create_customerlist_fragement();
        startActivity(new Intent(MainActivity.this, CustomerlistActivity.class));

    }

    private void showMoleculesTab() {
        // session.setLogin(false);

        // finish();
    }

    private void showMessageScreen() {
        // session.setLogin(false);
        Intent intent = new Intent(MainActivity.this, MessageActivity.class);
        startActivity(intent);
        // finish();
    }

    private void showloginscreen() {
        session.setLogin(false);
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        try {
             NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nMgr.cancel(NOTIFICATION_ID);
        } catch (Exception e) {
        }
        finish();
    }

    private void showlogin_delete_saveddata()
    {
        session.setLogin(false);
        //added by apurva to clear status after logout
        SharedPreferences pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("IS_TAKE_ORDER_ASSIGNED"); // will delete key key_name3
        editor.remove("IS_PAYMENT_COLLECTION_ASSIGNED");
        editor.remove("IS_ORDER_DELIVERY_ASSIGNED");// will delete key key_name4
        editor.clear();
        // Save the changes in SharedPreferences
        editor.commit();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        try {
            NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nMgr.cancel(NOTIFICATION_ID);
        } catch (Exception e) {
        }
        finish();
    }

    private void showLastOrders() {

        Intent intent = new Intent(MainActivity.this, Order_list.class);
        intent.putExtra(CHEMIST_IS_LAST_10_ORDER, true);
        startActivity(intent);
        // finish();
    }

    private void show_Users() {

        Intent intent = new Intent(MainActivity.this,DistributorUsers.class);
        startActivity(intent);

    }


    private void showDistributorcustomer() {

        Intent intent = new Intent(MainActivity.this, DistributorCustomerList.class);
        startActivity(intent);
    }

    private void showDistributorProductCatlog() {

      //  Intent intent = new Intent(MainActivity.this, InventorylistActivity.class);
        Intent intent = new Intent(MainActivity.this, DistributorProductcatlog.class);
        startActivity(intent);

    }

    private void showDistributorOrders() {

        Intent intent = new Intent(MainActivity.this, DistributorsOrderList.class);
        startActivity(intent);
    }


    private void showDistributorPendingBill() {

        Intent intent = new Intent(MainActivity.this, DistributorPendingBills.class);
        startActivity(intent);
    }

    private void showDistributorSalesandReturn() {

        Intent intent = new Intent(MainActivity.this, DistributorReturnsActivity.class);
        startActivity(intent);

    }




    private void showDistributorPayments() {

        Intent intent = new Intent(MainActivity.this, DistributorPayments.class);
        startActivity(intent);
    }

    private void showDistributorUsers() {

        Intent intent = new Intent(MainActivity.this, DistributorCustomerList.class);
       // intent.putExtra(CHEMIST_IS_LAST_10_ORDER, true);
        startActivity(intent);
    }


    private void showDistributorCallPlan() {

        Intent intent = new Intent(MainActivity.this, CallPlan.class);
       // intent.putExtra(CHEMIST_IS_LAST_10_ORDER, true);
        startActivity(intent);
    }



    private void showDistributorTrackUser() {

//        Intent intent = new Intent(MainActivity.this, Order_list.class);
//        intent.putExtra(CHEMIST_IS_LAST_10_ORDER, true);
//        startActivity(intent);

    }


    private void showDistributorNotification() {

        Intent intent = new Intent(MainActivity.this, DistributorNotificationActivity.class);
        startActivity(intent);

    }


    private void showDistributorLiveChat() {
        Intent intent = new Intent(MainActivity.this, StockistOrderDetails.class);
        startActivity(intent);

    }

    private void showLastOrdersSalesMan() {
        //Intent intent = new Intent(MainActivity.this, OrderHistoryActivity.class);
        Intent intent = new Intent(MainActivity.this, HistoryOrdersActivity.class);
        intent.putExtra(CHEMIST_IS_LAST_10_ORDER, true);
        startActivity(intent);
    }


    private void showedit() {

        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
        // finish();
    }
    private void showchangepassword() {
        globalVariable.setFromHomeActivity(true);
        Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
        // finish();
    }

    @Override
    public void onBackPressed() {
        if (_drawer.isDrawerOpen(GravityCompat.START)) {
            _drawer.closeDrawer(GravityCompat.START);
        } else {
            // super.onBackPressed();
            exit_notification();

        }
    }
    private String get_day_number(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String daynumber = "1";

        switch (day) {

            case Calendar.MONDAY:
                daynumber = "1";
                break;
            case Calendar.TUESDAY:
                daynumber = "2";
                break;

            case Calendar.WEDNESDAY:
                daynumber = "3";
                break;
            case Calendar.THURSDAY:
                daynumber = "4";
                break;
            case Calendar.FRIDAY:
                daynumber = "5";
                break;
            case Calendar.SATURDAY:
                daynumber = "6";
                break;

            case Calendar.SUNDAY:
                daynumber = "0";
                // daynumber = "7";
                // Current day is Sunday
                break;
        }
        return daynumber;
    }
    private void exit_notification() {
        new AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure want to Exit")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                        //  System.exit(0);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }

    private void exportDB() {
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source = null;
        FileChannel destination = null;
        String backupDBPath = "ordergenie";
        String currentDBPath = "/data/" + "com.synergy.ordergenie" + "/databases/" + backupDBPath;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void get_client_details()

    {
        String name_Header = pref.getString(ConstData.user_info.CLIENT_FULL_NAME, "OrderGenie");

       // _txt_profile_name.setText(pref.getString(ConstData.user_info.CLIENT_NAME, "OrderGenie"));
        //_txt_profile_name.setText(pref.getString(ConstData.user_info.CLIENT_FULL_NAME, "OrderGenie"));


        if (name_Header.equals("null"))
        {
            _txt_profile_name.setText("");
        }
        else
        {
            _txt_profile_name.setText(name_Header);
        }

        _txt_profile_email.setText(pref.getString(ConstData.user_info.EMAIL_ID, "www.ordergenie.co.in"));
        User_name = pref.getString(CLIENT_NAME, "OrderGenie");
    }

    private void call_refresh_data() {

        //String passEncrypted = pref.getString(encrypt("key"), encrypt(""));
        //String key = decrypt(passEncrypted);

        String key = pref.getString("key", "");
        //String key = decrypt(passEncrypted);
        globalVariable.setToken(key);


        Intent download_intent = new Intent(Intent.ACTION_SYNC, null, this, RefreshData.class);
        startService(download_intent);
    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {

        //  String s=response.toString();

    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {

        if (response != null) {
            Log.d("f_name", f_name);
            Log.d("DETAIL_FRAG_Responce11", String.valueOf(response)+"hii");
            try {
                if (f_name.equals(GET_STOCKIST_STASTICS)) {

                    JSONObject j_obj = response.getJSONObject(0);
                    Bundle b = new Bundle();
                    tx_customercount_value.setText(j_obj.getString("Total_customer_count"));
                    tx_pendingbills_value.setText(j_obj.getString("Total_pending_bills"));
                    tx_orders_value.setText(j_obj.getString("Total_orders"));
                    tx_sales_value.setText(j_obj.getString("Total_Sales"));
                    tx_returns_value.setText(j_obj.getString("Total_returns"));
                    // tx_inv_value.setText(j_obj.getString("Total_Inventory_count"));

                    text_int_total_vallue = j_obj.getString("Total_Inventory_count");

               //     Log.d("DETAIL_FRAG_Responce13", text_int_total_vallue);

                 //   Log.d("DETAIL_FRAG_Responce12", j_obj.getString("Total_customer_count") + "  " + j_obj.getString("Total_pending_bills") + "  " + j_obj.getString("Total_Sales") + "  " + j_obj.getString("Total_returns") + "  " + j_obj.getString("Total_orders") + "  " + j_obj.getString("Total_Inventory_count"));

                    b.putString("Total_Inventory_count", j_obj.getString("Total_Inventory_count"));

                }
                if (f_name.equals(AppConfig.GET_USER_OFFERS)) {
                    ((ChemistDashboardFragment) o_DashboardFragment).create_offer_sliding(response.toString());
                    response.toString();
                }

                if (f_name.equals(AppConfig.DISTRIBUTOR_DASHBOARD_DAYWISE_DATA)||f_name.equals(AppConfig.DISTRIBUTOR_DASHBOARD_WEEKWISE_DATA)||f_name.equals(AppConfig.DISTRIBUTOR_DASHBOARD_MONTHWISE_DATA)) {

                        try {
                        ((DistributorDashboardFragment) o_DashboardFragment).getRequest(response.toString(),f_name);
                        response.toString();

                        } catch (Exception e) {
                            Log.d("Excep", e.getMessage());
                        }
                    }

                    if (f_name.equals(AppConfig.GET_DISTRIBUTOR_MONTH_GRAPH))
                    {
                      Log.d("Graph",response.toString());

                        ((DistributorDashboardFragment) o_DashboardFragment).getRequest(response.toString(),f_name);
                        response.toString();
                    }

            } catch (Exception e) {
                e.toString();
            }
        }

    }


    @Override
    public void onFragmentLoaded(TextView tx_customercount_value, TextView tx_pendingbills_value, TextView tx_orders_value,
                                 TextView tx_sales_value, TextView tx_returns_value, TextView tx_inv_value) {
        this.tx_customercount_value = tx_customercount_value;
        this.tx_pendingbills_value = tx_pendingbills_value;
        this.tx_orders_value = tx_orders_value;
        this.tx_sales_value = tx_sales_value;
        this.tx_returns_value = tx_returns_value;
        this.tx_inv_value = tx_inv_value;
        tx_customercount_value.setText("");
    }

    @Override
    public void onResume() {
        super.onResume();

        imageLoader.clearDiskCache();
        imageLoader.clearMemoryCache();
        imageLoader.displayImage(pref.getString(CLIENT_PROFILE_IMAGE, ""), iv_profile_pic, options);

    }

    private void initImageLoader() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
                this).defaultDisplayImageOptions(defaultOptions).memoryCache(
                new WeakMemoryCache());

        ImageLoaderConfiguration config = builder.build();
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        imageLoader.init(config);
    }
    public  void UpdateDialog() {
        new get_current_location_ondashboard(MainActivity.this);
//Toast.makeText(getApplicationContext(),"something is not fishy",Toast.LENGTH_SHORT).show();

//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse("market://details?id=com.chemistproject.smartp"));
        try {
          //  startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),"Network Error. Please try again later.",Toast.LENGTH_SHORT).show();
            // intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.android.app"));
         //   intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.chemistproject.smartp"));
        }
    }

//    @Override
//    public void onFragmentLoaded(TextView tx_averagesales, TextView tx_averageorder) {
//
//    }
}
