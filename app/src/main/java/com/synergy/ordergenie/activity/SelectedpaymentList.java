package com.synergy.ordergenie.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toolbar;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.adapter.MakePaymentListAdapter;
import com.synergy.ordergenie.adapter.SelectedpaymentAdapter;
import com.synergy.ordergenie.model.m_pendingbills;

import java.util.ArrayList;
import java.util.List;

public class SelectedpaymentList extends AppCompatActivity {


    RecyclerView recyclerview;
    ArrayList<m_pendingbills> myList ;
    SelectedpaymentAdapter selectedpaymentAdapter;
    private TextView empty_view;
    List<m_pendingbills> posts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectedpayment_list);

        setTitle("Pending Bills");

        empty_view = (TextView) findViewById(R.id.empty_view);
        Bundle bundleobj = getIntent().getExtras();
        myList = (ArrayList<m_pendingbills>) bundleobj.getSerializable("selected_items");

       // myList = (ArrayList<m_pendingbills>) getIntent().getSerializableExtra("selected_items");

       // Log.d("print_myList", String.valueOf(myList));

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(),
                DividerItemDecoration.VERTICAL);
        recyclerview.addItemDecoration(dividerItemDecoration);


       fill_payment_list(myList);


        //getIntent().getParcelableArrayListExtra();
    }


    public  void  fill_payment_list(final List<m_pendingbills> posts_s)
    {

        if (posts_s.size() > 0) {
            empty_view.setVisibility(View.GONE);
        } else {
            empty_view.setVisibility(View.VISIBLE);
        }

        Log.d("size",String.valueOf(posts_s.size()));
        selectedpaymentAdapter = new SelectedpaymentAdapter(posts_s);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(selectedpaymentAdapter);
    }

}
