package com.synergy.ordergenie.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.synergy.ordergenie.BR;
import com.synergy.ordergenie.Helper.SQLiteHandler;
import com.synergy.ordergenie.R;
import com.synergy.ordergenie.model.m_stockist;
import com.synergy.ordergenie.utils.OGtoast;

import static com.synergy.ordergenie.utils.ConstData.data_refreshing.CHEMIST_STOCKISTLIST;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

public class StockistList extends AppCompatActivity {

    @BindView(R.id.rv_datalist)
    RecyclerView rv_datalist;
    List<m_stockist> posts;
    SharedPreferences pref;
    private SQLiteHandler db;

    public static final String CHEMIST_STOCKIST_ID = "chemist_stockist_id";
    public static final String CHEMIST_STOCKIST_NAME = "chemist_stockist_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stockist_list);
        db = new SQLiteHandler(this);
        ButterKnife.bind(this);
        pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_stockist_list, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_filter:
                show_dialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        get_stockist_json(pref.getString(CHEMIST_STOCKISTLIST, "[]"));
    }

    private void get_stockist_json(String jsondata) {
        // LoadJsonFromAssets _LoadJsonFromAssets = new LoadJsonFromAssets("Chemist_Stokist.json", StockistList.this);
        //  String jsondata = //_LoadJsonFromAssets.getJson();
        if (!jsondata.isEmpty()) {
            Log.e("Stockist",jsondata.toString());
            GsonBuilder builder = new GsonBuilder();

            Gson mGson = builder.create();

            posts = new ArrayList<m_stockist>();
            posts = Arrays.asList(mGson.fromJson(jsondata, m_stockist[].class));

            Collections.sort(posts, comparator);
            Collections.reverse(posts);

            fill_stockist(posts);

        }

    }

    Comparator<m_stockist> comparator = new Comparator<m_stockist>() {
        @Override
        public int compare(m_stockist o1, m_stockist o2) {
            boolean b1 = o1.getStatus();
            boolean b2 = o2.getStatus();
            if (b1 && !b2) {
                return +1;
            }
            if (!b1 && b2) {
                return -1;
            }
            return 0;
        }
    };


    private void fill_stockist(final List<m_stockist> posts_s) {

        final RecyclerView.Adapter<BindingViewHolder> adapter = new RecyclerView.Adapter<BindingViewHolder>() {
            @Override
            public BindingViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
                LayoutInflater inflater = LayoutInflater.from(StockistList.this);
                ViewDataBinding binding = DataBindingUtil
                        .inflate(inflater, R.layout.adpter_stockist_list, parent, false);


                return new BindingViewHolder(binding.getRoot());
            }

            @Override
            public void onBindViewHolder(final BindingViewHolder holder, final int position) {
                final m_stockist stockist_list = posts_s.get(position);
                holder.getBinding().setVariable(BR.v_stockistlist, stockist_list);
                holder.getBinding().executePendingBindings();

                if (stockist_list.getStockist_Id() != null) {
                    holder.getBinding().getRoot().setTag(stockist_list.getStockist_Id());
                }

                if (stockist_list.getStatus() != true) {
                    holder.getBinding().getRoot().findViewById(R.id.status).setVisibility(View.VISIBLE);
                    ((ImageView) holder.getBinding().getRoot().findViewById(R.id.arrow)).setImageResource(R.drawable.lock);

                } else {
                    holder.getBinding().getRoot().findViewById(R.id.status).setVisibility(View.GONE);
                    ((ImageView) holder.getBinding().getRoot().findViewById(R.id.arrow)).setImageResource(R.drawable.right_arrow_grey);
                }


                holder.getBinding().getRoot().setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (stockist_list.getStatus() != true) {
                            request_stock_access(posts_s.get(position).getStockist_Id().toString(), posts_s.get(position).getStokist_Name());
                        } else {

                            Create_new_order(posts_s.get(position).getStockist_Id().toString(), posts_s.get(position).getStokist_Name().toString());
                        }
                    }
                });

            }

            @Override
            public int getItemCount() {
                return posts_s.size();
            }
        };

        rv_datalist.setLayoutManager(new LinearLayoutManager(this));
        rv_datalist.setAdapter(adapter);
    }


    private class BindingViewHolder extends RecyclerView.ViewHolder {

        public BindingViewHolder(View itemView) {
            super(itemView);
        }

        public ViewDataBinding getBinding() {
            return DataBindingUtil.getBinding(itemView);
        }
    }

    private void request_stock_access(String stockist_id, String stockist_name) {
        Intent intent = new Intent(this, RequestStockistAccess.class);
        intent.putExtra(CHEMIST_STOCKIST_ID, stockist_id);
        intent.putExtra(CHEMIST_STOCKIST_NAME, stockist_name);
        startActivity(intent);
    }

    private void Create_new_order(String stockist_id, String stockist_name) {
        Intent intent = new Intent(this, Create_Order.class);
        intent.putExtra(CHEMIST_STOCKIST_ID, stockist_id);
        intent.putExtra(CHEMIST_STOCKIST_NAME, stockist_name);
        startActivity(intent);
    }

    private void show_dialog() {

        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogview = inflater.inflate(R.layout.dialog_chemist_stockist_filter, null);
        final Dialog infoDialog = new Dialog(this);//builder.create();
        infoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        infoDialog.setContentView(dialogview);
        infoDialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        final TextView med_name = (TextView) dialogview.findViewById(R.id.med_name);
        final TextView location_txt = (TextView) dialogview.findViewById(R.id.location_txt);
        dialogview.findViewById(R.id.filter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!med_name.getText().toString().isEmpty()) {
                    try {


                        filter_data_on_stockist(med_name.getText().toString());
                        infoDialog.dismiss();
                    }
                    catch (Exception e)
                    {

                    }
                } else if (!location_txt.getText().toString().isEmpty()) {
                    filter_on_location(location_txt.getText().toString());
                }

            }
        });

        dialogview.findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoDialog.dismiss();
                fill_stockist(posts);
            }
        });

        set_attributes(infoDialog);
        infoDialog.show();

    }

    private void set_attributes(Dialog dlg) {

        Window window = dlg.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        Display mdisp = getWindowManager().getDefaultDisplay();
        Point mdispSize = new Point();
        mdisp.getSize(mdispSize);

        int[] textSizeAttr = new int[]{android.R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedValue typedValue = new TypedValue();
        TypedArray a = this.obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionbarsize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();

        int maxX = mdispSize.x;
        int maxY = mdispSize.y;

        wlp.gravity = Gravity.TOP | Gravity.LEFT;
        wlp.x = maxX;   //x position
        wlp.y = actionbarsize - 20;   //y position
        // wlp.width = 50;
        // wlp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

    }

    void filter_data_on_stockist(String medicine_name) {
        Cursor crs = db.get_stockist_id_on_medicine_name(medicine_name);

        final List<m_stockist> filteredModelList = new ArrayList<>();


        if (crs.moveToFirst()) {
            do {


                for (m_stockist model : posts) {
                    if (model.getStockist_Id() == crs.getInt(crs.getColumnIndex("Stockist_id")))
                        filteredModelList.add(model);

                }

            } while (crs.moveToNext());
        }


        fill_stockist(filteredModelList);

    }

    void filter_on_location(String location) {

        try {
            final List<m_stockist> filteredModelList = new ArrayList<m_stockist>();
            for (m_stockist model : posts) {
                if (model.getLocation().toLowerCase().equals(location.toLowerCase())) {
                    filteredModelList.add(model);
                }

            }
            fill_stockist(filteredModelList);
        } catch (Exception e) {
            OGtoast.OGtoast("Location not updated", this);
        }
    }

}
