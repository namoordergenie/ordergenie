package com.synergy.ordergenie.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.io.Serializable;

/**
 * Created by prakash on 17/07/16.
 */
public class m_pendingbills implements Serializable {


    String Invoiceno;
    String Invoicedate;
    Integer Totalitems;
    String Billamount;
    String Paymentreceived;
    String Balanceamt;
    String InvoiceID;
    private boolean isSelected;


    public m_pendingbills(String invoiceno, String invoicedate, Integer totalitems,
                          String billamount, String paymentreceived, String balanceamt, String invoiceID) {
        Invoiceno = invoiceno;
        Invoicedate = invoicedate;
        Totalitems = totalitems;
        Billamount = billamount;

        Paymentreceived = paymentreceived;
        Balanceamt = balanceamt;
        InvoiceID = invoiceID;
    }


    public String getInvoiceno() {
        return Invoiceno;
    }

    public void setInvoiceno(String invoiceno) {
        Invoiceno = invoiceno;
    }

    public String getInvoicedate() {
        return Invoicedate;
    }

    public void setInvoicedate(String invoicedate) {
        Invoicedate = invoicedate;
    }

    public Integer getTotalitems() {
        return Totalitems;
    }

    public void setTotalitems(Integer totalitems) {
        Totalitems = totalitems;
    }

    public String getBillamount() {
        return Billamount;
    }

    public void setBillamount(String billamount) {
        Billamount = billamount;
    }

    public String getPaymentreceived() {
        return Paymentreceived;
    }

    public void setPaymentreceived(String paymentreceived) {
        Paymentreceived = paymentreceived;
    }

    public String getBalanceamt() {
        return Balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        Balanceamt = balanceamt;
    }

    public String getInvoiceID() {
        return InvoiceID;
    }

    public void setInvoiceID(String invoiceID) {
        InvoiceID = invoiceID;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


}
