package com.synergy.ordergenie.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.model.DistributorpaymentListmodal;
import com.synergy.ordergenie.model.m_pendingbills;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Admin on 04-12-2017.
 */

public class MakePaymentListAdapter extends RecyclerView.Adapter<MakePaymentListAdapter.MyViewHolder> {

    private List<m_pendingbills> paymentList;
    private List<m_pendingbills> selectedpaymentList = new ArrayList<m_pendingbills>();
    private SelectedListItem selectedListItem;
    String time;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date_textview, textview_invoicenum, textview_time, ruppees_textview;
        CheckBox txtcheckBox;

        public MyViewHolder(View view) {
            super(view);
            txtcheckBox = (CheckBox) view.findViewById(R.id.checkBox);
            date_textview = (TextView) view.findViewById(R.id.date_textview);
            textview_invoicenum = (TextView) view.findViewById(R.id.txt_invoice);
            textview_time = (TextView) view.findViewById(R.id.textview_time);
            ruppees_textview = (TextView) view.findViewById(R.id.ruppees_textview);
        }
    }

    public MakePaymentListAdapter(List<m_pendingbills> paymentList, SelectedListItem selectedListItem) {
        this.paymentList = paymentList;
        this.selectedListItem = selectedListItem;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.make_paymentlist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final m_pendingbills dpaymentListmodal = paymentList.get(position);

        holder.date_textview.setText(dpaymentListmodal.getInvoicedate());
        holder.textview_invoicenum.setText("INVOICE NO:" + dpaymentListmodal.getInvoiceno());

        holder.ruppees_textview.setText("Rs." + String.valueOf(dpaymentListmodal.getBillamount()));

        if (dpaymentListmodal.getInvoicedate() != null) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            try {
                Date d1 = sdf.parse(dpaymentListmodal.getInvoicedate());
                final String OLD_FORMAT = "yyyyMMdd";
                ;
                sdf.applyPattern(OLD_FORMAT);
                String newdate = sdf.format(d1);
                Log.d("timepayment11", d1.toString());
                Log.d("timepayment12", newdate);
                Log.d("timepayment13", String.valueOf(d1.getTime()));
                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                time = formatter.format(new Date(d1.getTime()));
                Log.d("timepayment14", time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.textview_time.setText(time);
        holder.txtcheckBox.setChecked(dpaymentListmodal.isSelected());

        holder.txtcheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int position = holder.getAdapterPosition();
                Log.d("position", String.valueOf(position));
                paymentList.get(position).setSelected(isChecked);
                if (isChecked) {
                    selectedpaymentList.add(paymentList.get(position));
                    Log.d("selectedpaymentList", selectedpaymentList.toString());
                    Log.d("Size", String.valueOf(selectedpaymentList.size()));
                } else {
                    selectedpaymentList.remove(paymentList.get(position));
                    Log.d("Size", String.valueOf(position) + "position removed");
                }

                selectedListItem.setValues(selectedpaymentList);
            }
        });

    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    public interface SelectedListItem {
        public void setValues(List<m_pendingbills> al);
    }
}

