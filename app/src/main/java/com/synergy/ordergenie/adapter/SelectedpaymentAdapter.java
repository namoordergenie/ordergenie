package com.synergy.ordergenie.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.model.m_pendingbills;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by Admin on 21-02-2018.
 */

public class SelectedpaymentAdapter extends RecyclerView.Adapter<SelectedpaymentAdapter.MyViewHolder> {

    private List<m_pendingbills> paymentsList;
    String time;

    public class MyViewHolder extends RecyclerView.ViewHolder {
       public TextView invoicenumber_text, date_text,textview_items,rupees_text,text_invoicetime;

        public MyViewHolder(View view) {
            super(view);
            date_text = (TextView) view.findViewById(R.id.date_text);
            invoicenumber_text = (TextView) view.findViewById(R.id.invoicenumber_text);
            textview_items = (TextView) view.findViewById(R.id.textview_items);
            rupees_text = (TextView) view.findViewById(R.id.rupees_text);
            text_invoicetime = (TextView) view.findViewById(R.id.text_invoicetime);

        }
    }


    public SelectedpaymentAdapter(List<m_pendingbills> paymentsList) {
        this.paymentsList = paymentsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_list_row, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        m_pendingbills mm_pendingbills = paymentsList.get(position);

        holder.invoicenumber_text.setText(mm_pendingbills.getInvoiceno());
        holder.rupees_text.setText(mm_pendingbills.getBillamount());
        holder.date_text.setText(mm_pendingbills.getInvoicedate());
        holder.textview_items.setText(String.valueOf(mm_pendingbills.getTotalitems()));

        if (mm_pendingbills.getInvoicedate() != null) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            try {
                Date d1 = sdf.parse(mm_pendingbills.getInvoicedate());
                final String OLD_FORMAT = "yyyyMMdd";
                ;
                sdf.applyPattern(OLD_FORMAT);
                String newdate = sdf.format(d1);
                Log.d("timepayment11", d1.toString());
                Log.d("timepayment12", newdate);
                Log.d("timepayment13", String.valueOf(d1.getTime()));
                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                time = formatter.format(new Date(d1.getTime()));
                Log.d("timepayment14", time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.text_invoicetime.setText(time);




//        holder.title.setText(mm_pendingbills.getTitle());
//        holder.genre.setText(mm_pendingbills.getGenre());
//        holder.year.setText(mm_pendingbills.getYear());

    }

    @Override
    public int getItemCount() {

        //return 0;
        return paymentsList.size();
    }
}
