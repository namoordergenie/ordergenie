package com.synergy.ordergenie.utils;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.synergy.ordergenie.Helper.SQLiteHandler;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;
import com.synergy.ordergenie.database.ChemistCart;
import com.synergy.ordergenie.database.DaoSession;
import com.synergy.ordergenie.database.MasterPlacedOrder;
import com.synergy.ordergenie.database.MasterPlacedOrderDao;

import java.util.List;

import static com.synergy.ordergenie.utils.ConstData.data_refreshing.CHEMIST_LAST_DATA_SYNC;
import static com.synergy.ordergenie.utils.ConstData.data_refreshing.CHEMIST_ORDERLIST;
import static com.synergy.ordergenie.utils.ConstData.data_refreshing.CHEMIST_PENDING_BILLS;
import static com.synergy.ordergenie.utils.ConstData.data_refreshing.CHEMIST_STOCKISTLIST;
import static com.synergy.ordergenie.utils.ConstData.decrypt;
import static com.synergy.ordergenie.utils.ConstData.encrypt;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_CITY_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_PASSWORD;
import static com.synergy.ordergenie.utils.ConstData.user_info.CLIENT_USERNAME;
import static com.synergy.ordergenie.utils.ConstData.user_info.STOCKIST_ID;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

/**
 * Created by 1132 on 14-11-2016.
 */

public class RefreshData extends IntentService implements MakeWebRequest.OnResponseSuccess {

    SharedPreferences pref;
    private SQLiteHandler db;
    AppController globalVariable;
    ConnectionDetector connectiondetector;
    public static final String ACTION_CONFIRM_ORDER = "android.ordergenie.intent.action.CONFIRM_ORDER";

    public RefreshData() {
        super(RefreshData.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent.getAction().equals(ACTION_CONFIRM_ORDER)) {

            if (ConnectionDetector.isConnectingToInternet(this)) {
                confirm_order();
            }
        } else {

            pref = getApplicationContext().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
            // connectiondetector = new ConnectionDetector(this);
            globalVariable = (AppController) getApplicationContext();
            db = new SQLiteHandler(this);

            if (ConnectionDetector.isConnectingToInternet(this)) {
                get_user_credentials();

            } else {
                // OGtoast.OGtoast( "Network not available",this);
            }

        }
    }

    void get_refreshed_data(String client_id, String city_id, String Stockist_id, String user_id) {
        MakeWebRequest.MakeWebRequest("get", AppConfig.GET_CHEMIST_STOCKIST,
                AppConfig.GET_CHEMIST_STOCKIST + "[" + client_id + "," + city_id + "]", this, false);

      /*  MakeWebRequest.MakeWebRequest("get", AppConfig.GET_STOCKIST_INDIVIDUAL_ORDER_HISTORY,
                AppConfig.GET_STOCKIST_INDIVIDUAL_ORDER_HISTORY+"["+Stockist_id+","+user_id+"]" , this, false);
*/

        MakeWebRequest.MakeWebRequest("get", AppConfig.GET_CHEMIST_ORDERLIST,
                AppConfig.GET_CHEMIST_ORDERLIST + client_id, this, false);

        MakeWebRequest.MakeWebRequest("get", AppConfig.GET_CHEMIST_PEDNING_BILLS,
                AppConfig.GET_CHEMIST_PEDNING_BILLS + client_id, this, false);

        MakeWebRequest.MakeWebRequest("get", AppConfig.GET_PARTIAL_CHEMIST_DATA,
                AppConfig.GET_PARTIAL_CHEMIST_DATA + "[" + client_id + ",\"" + pref.getString(CHEMIST_LAST_DATA_SYNC, "") + "\"]", this, false);

        try {

            JSONObject j_obj = new JSONObject();
            j_obj.put("chemistId", client_id);

            JSONArray j_arr = new JSONArray();
            j_arr.put(j_obj);

            MakeWebRequest.MakeWebRequest("out_array", AppConfig.GET_ALL_LEGEND_DATA, AppConfig.GET_ALL_LEGEND_DATA,
                    null, this, false, j_obj.toString());

        } catch (Exception e) {

        }

    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {
        try {
            if (response != null) {

                Log.e("saiiii", response.toString());

                if (f_name.equals(AppConfig.POST_CHEMIST_CONFIRM_ORDER)) {

                    DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
                    MasterPlacedOrderDao masterPlacedOrderDao = daoSession.getMasterPlacedOrderDao();
                    masterPlacedOrderDao.deleteOrder(response.getString("DOC_NO"));
                }

                if (f_name.equals(AppConfig.checklogin)) {
                    globalVariable.setToken(response.getString("token"));
                    SharedPreferences preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    //editor.putString(encrypt("key"), encrypt(Token));
                    editor.putString("key", response.getString("token"));
                    editor.commit();
                    confirm_order();
                    get_refreshed_data(pref.getString(CLIENT_ID, ""), pref.getString(CLIENT_CITY_ID, ""), pref.getString(STOCKIST_ID, ""), pref.getString(USER_ID, ""));
                }

            } else {
                // OGtoast.OGtoast( "Unable to connect to the server",getBaseContext());
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public void onSuccess_json_array(String f_name, JSONArray response) {
        if (response != null) {
            if (f_name.equals(AppConfig.GET_PARTIAL_CHEMIST_DATA)) {
                response.toString();
            }

            if (f_name.equals(AppConfig.GET_CHEMIST_STOCKIST)) {
                SharedPreferences.Editor edt = pref.edit();
                edt.putString(CHEMIST_STOCKISTLIST, response.toString());
                edt.commit();
            }

            if (f_name.equals(AppConfig.GET_CHEMIST_ORDERLIST)) {

                Log.e("Response333", response.toString());
                SharedPreferences.Editor edt = pref.edit();
                edt.putString(CHEMIST_ORDERLIST, response.toString());
                edt.commit();
            }

            if (f_name.equals(AppConfig.GET_CHEMIST_PEDNING_BILLS)) {
                SharedPreferences.Editor edt = pref.edit();
                edt.putString(CHEMIST_PENDING_BILLS, response.toString());
                edt.commit();
            }
            if (f_name.equals(AppConfig.GET_ALL_LEGEND_DATA)) {

                try {
                    Log.e("Legend", response.toString());

                    for (int j = 0; j < response.length(); j++) {
                        Boolean isDataavailable = false;
                        Cursor crs = db.get_legend_data(response.getJSONObject(j).getString("ClientID"));

                        if (crs != null && crs.getCount() > 0) {
                            while (crs.moveToNext()) {

                                isDataavailable = true;
                            }
                        }

                        if (!isDataavailable) {

                            for (int i = 0; i < response.getJSONObject(0).getJSONArray("stk_legends").length(); i++) {

                                db.insert_into_stockist_legend(response.getJSONObject(j).getString("ClientID"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("LegendName"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("ColorCode"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("StartRange"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("EndRange"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("StockLegendID"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("LegendMode"));
                            }
                        } else {
                            db.delete_legend_data(response.getJSONObject(j).getString("ClientID"));

                            for (int i = 0; i < response.getJSONObject(0).getJSONArray("stk_legends").length(); i++) {

                                db.insert_into_stockist_legend(response.getJSONObject(j).getString("ClientID"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("LegendName"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("ColorCode"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("StartRange"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("EndRange"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("StockLegendID"),
                                        response.getJSONObject(j).getJSONArray("stk_legends").getJSONObject(i).getString("LegendMode"));
                            }

                        }

                    }
                } catch (Exception e) {

                }

            }


        } else {
          /*  OGtoast.OGtoast( "Unable to connect to the server",
                    getBaseContext());*/
        }

    }

    void confirm_order() {
/*
       Cursor crs_order_json=db.get_order_json();


        if (crs_order_json != null && crs_order_json.getCount() > 0) {
            try {
                if (crs_order_json.moveToFirst()) {
                    do {

                       String SjArray=crs_order_json.getString(crs_order_json.getColumnIndex("json"));
                        Log.e("SjArrayyy",SjArray);

                        MakeWebRequest.MakeWebRequest("Post", AppConfig.POST_CHEMIST_CONFIRM_ORDER, AppConfig.POST_CHEMIST_CONFIRM_ORDER,
                                new JSONArray(SjArray), this,false,"");

                    } while (crs_order_json.moveToNext());
                }

            }catch (Exception e)
            {
               e.toString();
            }
        }
*/

        DaoSession daoSession = ((AppController) getApplication()).getDaoSession();
        MasterPlacedOrderDao masterPlacedOrderDao = daoSession.getMasterPlacedOrderDao();
        List<MasterPlacedOrder> masterPlacedOrderList = masterPlacedOrderDao.loadAll();


        for (MasterPlacedOrder masterPlacedOrder : masterPlacedOrderList) {

            String SjArray = masterPlacedOrder.getJson();
            Log.e("SjArrayyy", SjArray);

            try {
                MakeWebRequest.MakeWebRequest("Post", AppConfig.POST_CHEMIST_CONFIRM_ORDER, AppConfig.POST_CHEMIST_CONFIRM_ORDER,
                        new JSONArray(SjArray), this, false, "");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    void get_user_credentials() {

        String userEncrypted = pref.getString(CLIENT_USERNAME, encrypt(""));
        String passEncrypted = pref.getString(CLIENT_PASSWORD, encrypt(""));
        String pass = decrypt(passEncrypted);
        String user = decrypt(userEncrypted);

        try {
            JSONObject jsonParams = new JSONObject();

            jsonParams.put("email", user);
            jsonParams.put("password", pass);


            globalVariable.setToken(null);
            MakeWebRequest.MakeWebRequest("Post", AppConfig.checklogin, AppConfig.checklogin, jsonParams, this, false);
        } catch (Exception E) {

        }
    }

}





