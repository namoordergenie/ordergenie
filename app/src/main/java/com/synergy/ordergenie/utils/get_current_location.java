package com.synergy.ordergenie.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.app.AppConfig;
import com.synergy.ordergenie.app.AppController;

import static android.content.Context.MODE_PRIVATE;
import static com.synergy.ordergenie.utils.ConstData.user_info.USER_ID;
import static com.synergy.ordergenie.utils.SessionManager.PREF_NAME;

/**
 * Created by 1132 on 16-01-2017.
 */

public class get_current_location implements LocationListener, MakeWebRequest.OnResponseSuccess {
    private Context ctx;
    LocationManager mLocationManager;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private static final long MIN_TIME_BW_UPDATES = 10 * 1000;
    private TextView txt_address;
    private ProgressBar pgr;

    private SharedPreferences pref;
    AppController globalVariable;

    public get_current_location(Activity ctx) {
        this.ctx = ctx;
        pref = ctx.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        globalVariable = (AppController) ctx.getApplicationContext();
        get_location();
    }


    void get_location() {

        show_location_dialog(globalVariable.getFromMenuItemClick());
        //  isRequestCurrentLocation=true;
        mLocationManager = (LocationManager) ctx.getSystemService(ctx.LOCATION_SERVICE);
        //  mLocationManager.getLastKnownLocation();
        Boolean isGPSEnabled = mLocationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPSEnabled) {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);


            Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {

                String adress = LocationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(), ctx);
                if (adress != null) {
                    txt_address.setText(adress);
                   // txt_address1.setText(LocationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(), ctx));
                    pgr.setVisibility(View.GONE);
                }
            }

        } else {
            showSettingsAlert();
        }

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);

        alertDialog.setTitle("GPS Not Enabled");

        alertDialog.setMessage("Please enable the Location");

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ctx.startActivity(intent);
            }
        });


        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                OGtoast.OGtoast("Location Services not enabled !. Unable to get the location", ctx);
                dialog.cancel();
            }
        });
                alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            String adress = LocationAddress.getAddressFromLocation(location.getLatitude(), location.getLongitude(), ctx);
            if (adress != null)
            {
                txt_address.setText(adress);
                pgr.setVisibility(View.GONE);
            }

            JSONObject jsonParams = new JSONObject();

            try {

                jsonParams.put("UserID", pref.getString(USER_ID, "0"));

                jsonParams.put("Latitude", String.valueOf(location.getLatitude()));
                jsonParams.put("Longitude", String.valueOf(location.getLongitude()));
                jsonParams.put("CurrentLocation", adress);

              /*  jsonParams.put("Latitude", "20.903118");
                jsonParams.put("Longitude", "74.774986");
                jsonParams.put("CurrentLocation", "Dhule, Near Songir\n424002\nIndia");*/
                Log.e("location11", adress);
                Log.e("location12", jsonParams.toString());
                //globalVariable.setToken(null);
                MakeWebRequest.MakeWebRequest("Post", AppConfig.POST_UPDATE_USER_LOCATION, AppConfig.POST_UPDATE_USER_LOCATION, jsonParams, ctx, false);

            } catch (Exception e) {
            }
        }
    }
    @Override
    public void onSuccess_json_array(String f_name, JSONArray response)
    {

    }

    @Override
    public void onSuccess_json_object(String f_name, JSONObject response) {

        Log.e("LocationUpdate", response.toString());
        Log.e("location13", response.toString());


    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override

    public void onProviderEnabled(String provider) {

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {


    }


    void show_location_dialog(boolean isTrue) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        final View dialogview = inflater.inflate(R.layout.dialog_current_location, null);
        final Dialog infoDialog = new Dialog(ctx);//builder.create();
        infoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        infoDialog.setContentView(dialogview);
        txt_address = (TextView) dialogview.findViewById(R.id.txt_address);
      //  txt_address1 = (TextView) dialogview.findViewById(R.id.txt_address1);
        pgr = (ProgressBar) dialogview.findViewById(R.id.prgr_bar);
        pgr.setVisibility(View.VISIBLE);
        dialogview.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                infoDialog.dismiss();
            }
        });

        if (isTrue) {
            infoDialog.show();
        } else {
            infoDialog.hide();
        }
    }

//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
}
