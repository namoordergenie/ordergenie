package com.synergy.ordergenie.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.OnClick;

import com.synergy.ordergenie.R;
import com.synergy.ordergenie.activity.AllPendingBills;
import com.synergy.ordergenie.activity.CallPlan;
import com.synergy.ordergenie.activity.CustomerlistActivity;
import com.synergy.ordergenie.activity.DistributorCustomerDetails;
import com.synergy.ordergenie.activity.DistributorCustomerList;
import com.synergy.ordergenie.activity.DistributorPendingBills;
import com.synergy.ordergenie.activity.DistributorReturnsActivity;
import com.synergy.ordergenie.activity.HistoryOrdersActivity;
import com.synergy.ordergenie.activity.InventorylistActivity;
import com.synergy.ordergenie.activity.MainActivity;
import com.synergy.ordergenie.activity.OrderHistoryActivity;
import com.synergy.ordergenie.activity.SalesReturnActivity;
import com.synergy.ordergenie.activity.StockistOrderDetails;
import com.synergy.ordergenie.adapter.DistributorSalesExpanListAdapter;


public class StockistDashboardFragment extends Fragment implements View.OnClickListener {

    ImageButton btn;
    TextView textView, textView_userName;

    private OnmenuitemSelected mListener;

    public static StockistDashboardFragment newInstance() {
        StockistDashboardFragment fragment = new StockistDashboardFragment();
        return fragment;
    }

    public StockistDashboardFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnmenuitemSelected) {
            mListener = (OnmenuitemSelected) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement OnmenuitemSelected.");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v_view = inflater.inflate(R.layout.stockist_fragment_dashboard, container, false);

        textView = (TextView) v_view.findViewById(R.id.tx_inv_value);


        textView_userName = (TextView) v_view.findViewById(R.id.txt_stcockist);


        MainActivity set_UserName = (MainActivity) getActivity();

        String user_profileName = set_UserName.User_name;

        textView_userName.setText(user_profileName);

     //   Log.d("USERPROFILENAME_IS", user_profileName);

        MainActivity asdfdffffff = (MainActivity) getActivity();

        String total_Inventory_Value = asdfdffffff.text_int_total_vallue;

        textView.setText(total_Inventory_Value);

    //    Log.d("printttingvalue", total_Inventory_Value);
        //v_view.getId();

        //btn = (ImageButton) v_view.findViewById(R.id.ib_customer);
        //btn.setOnClickListener((View.OnClickListener) StockistDashboardFragment.this);

        ButterKnife.bind(this, v_view);
        return v_view;
    }


//	@OnClick(R.id.submit)
//	public void sayHi(Button button) {
//		button.setText("Hello!");
//	}

//	@OnClick(R.id.submit)
//	public void submit() {
//		// TODO submit data to server...
//	}

    @OnClick(R.id.ib_customer)
    public void customerlist() {
        create_customeractivity();
    }

    @OnClick(R.id.LL_Inventory)
    public void Inventory(View view) {
        create_Inventorysactivity();
    }


    @OnClick(R.id.lnr_call_plan)
    public void callplan(View view) {
        Show_CallPlan();
    }


    @OnClick(R.id.pending_fragment)
    public void pendingbills(View view) {
        create_pendingbillsactivity();
    }

    @OnClick(R.id.orders_fragment)
    public void orderhistory(View view) {
        Show_Order();
    }


    @Override
    public void onClick(View v) {
        // implements your things
        mListener.OnmenuitemSelected(1, "", "", "");

        //create_customeractivity();
    }

    @OnClick(R.id.customer_fragment)
    public void onClick() {
        create_customeractivity();
    }


    public interface OnmenuitemSelected {
        void OnmenuitemSelected(int imageResId, String name, String description, String url);
    }


    private void create_customeractivity() {
        Intent intent = new Intent(getActivity(), CustomerlistActivity.class);
        startActivity(intent);


    }

    private void create_salesreturnactivity() {
        Intent intent = new Intent(getActivity(), SalesReturnActivity.class);
        startActivity(intent);

    }

    private void create_pendingbillsactivity() {
//		Intent intent = new Intent(getActivity(), AllPendingBills.class);
//		startActivity(intent);
//        Intent intent = new Intent(getActivity(), AllPendingBills.class);

        Intent intent = new Intent(getActivity(), AllPendingBills.class);
        startActivity(intent);

    }

    private void create_Inventorysactivity() {
        Intent intent = new Intent(getActivity(), InventorylistActivity.class);
        startActivity(intent);

    }

    private void create_Orderhistory() {
        Intent intent = new Intent(getActivity(), OrderHistoryActivity.class);
        startActivity(intent);
    }

    private void Show_Order() {
        //Intent intent=new Intent(getActivity(),OrderHistoryActivity.class);
        Intent intent = new Intent(getActivity(), HistoryOrdersActivity.class);
        startActivity(intent);
    }

    private void Show_CallPlan() {

        Intent intent = new Intent(getActivity(), CallPlan.class);
        startActivity(intent);
    }


}
